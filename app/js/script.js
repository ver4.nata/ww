$(document).ready(function(){

    var show = true;

    var countbox = "#counts";
    if (document.querySelector('#counts')){
        $(window).on("scroll load resize", function(){
            if(!show) return false;                   // Отменяем показ анимации, если она уже была выполнена
            var w_top = $(window).scrollTop();        // Количество пикселей на которое была прокручена страница
            var e_top = $(countbox).offset().top;     // Расстояние от блока со счетчиками до верха всего документа
            var w_height = $(window).height();        // Высота окна браузера
            var d_height = $(document).height();      // Высота всего документа
            var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
            if(w_top + 200 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height){
                $(".spincrement").spincrement({
                    thousandSeparator: "",
                    duration: 1200
                });
                show = false;
            }
        });

    }


    var grid = document.querySelector('.grid');
    var msnry;


    imagesLoaded( grid, function() {
        // init Isotope after all images have loaded
        msnry = new Masonry( grid, {
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true
        });

    });

    // init Isotope
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        layoutMode: 'fitRows',
        getSortData: {
            name: '.name',
            symbol: '.symbol',
            number: '.number parseInt',
            category: '[data-category]',
        }
    });

// filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
        }
    };

// bind filter button click
    $('#filters-masonry').on( 'click', 'button', function() {
        var filterValue = $( this ).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[ filterValue ] || filterValue;
        $grid.isotope({ filter: filterValue });
    });

// bind sort button click
    $('#sorts').on( 'click', 'button', function() {
        var sortByValue = $(this).attr('data-sort-by');
        $grid.isotope({ sortBy: sortByValue });
    });

// change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $( this ).addClass('is-checked');
        });
    });

    $('.portfolios').filterData({
        aspectRatio: '8:5',
        nOfRow : 3, // the number of rows
        itemDistance : 0
    });

});


(function () {
    $('.slick-clients').slick({
        adaptiveHeight: true,
        dots: true,
        arrows:false,
    });
})();
(function () {
    $('.slick-carousel').slick({
        adaptiveHeight: true,
        dots: true,
        arrows:false,
    });
})();


(function () {
    $('.slick-posts').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay:false
    });
})();
(function () {
    $('.slick-team').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay:true
    });
})();

(function () {
    $('.slick-team').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay:true
    });
})();


// ( function() {
//
//     $(window).scroll(function () {
//         if ($(this).scrollTop() > 200) {
//             $('.scroll-up').fadeIn();
//         } else {
//             $('.scroll-up').fadeOut();
//         }
//     });
//
// })();







var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}

// scroll up-------------------------------------
( function() {
    let scrollUp = document.body.querySelector('.scroll-up');

    $(scrollUp).click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        return false;
    });
    // $(window).scroll(function () {
    //     if (document.body.scrollTop > 50 || scrollUp.scrollTop > 50) {
    //         scrollUp.style.display = "block";
    //
    //     } else {
    //         scrollUp.style.display = "none";
    //     }
    // });

})();
// -----------------------------------------------

