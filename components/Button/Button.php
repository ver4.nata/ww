<?php

function button($text, $link, $class, $style){

    if(!empty($link)){

        echo <<<HTML
    
        <a href="{$link}" class="{$class}" style="{$style}">{$text}</a>
HTML;

    } else {
        echo <<<HTML
    
    <button class="{$class}" style="{$style}">{$text}</button>

HTML;

    }
}