/* jshint esversion: 6, node: true */
'use strict';

const gulp = require('gulp'),
    connect = require('./index.js'),
    sass         = require('gulp-sass'),
    // autoprefixer = require('gulp-autoprefixer'),
    // rename       = require('gulp-rename'),
    // cssnano = require('gulp-cssnano'),
    browserSync = require('browser-sync');

gulp.task('sass', function() {
    return gulp.src("app/sass/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream());
});


//task that fires up php server at port 8001
gulp.task('connect', function (callback) {
    connect.server({
        port: 8001
    }, callback);
});


//task that watch change files
gulp.task('watcher', function () {
    gulp.watch('**/*.scss', ['sass']);
    gulp.watch('app/**/*.js').on("change", browserSync.reload);
    gulp.watch('*.html').on('change', browserSync.reload);

});

//task that fires up browserSync proxy after connect server has started
gulp.task('browser-sync', ['connect'], function () {
    browserSync({
        proxy: '127.0.0.1:8001',
        port: 8910
    });
    gulp.watch(['**/*.php'], browserSync.reload);
});








//default task that runs task browser-sync ones and then watches php files to change. If they change browserSync is reloaded
gulp.task('default', ['browser-sync','watcher']);