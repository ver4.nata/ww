<?php include PATCH . "resources/view/component/home-bisnisess.html"; ?>

<!-- Main container start -->
<main class="main-content home nome-bisnisess">
    <!-- Carousel HOME-2 Start-->
    <section class="s-carousel">

        <div class="slick-carousel">
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/action.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>

                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('add ', '#', 'button  is-primary', '') ?>
                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'button  is-primary', '') ?>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Carousel HOME-2 end-->

    <!-- Services Start-->
    <section class="s-services">
        <div class="container">
            <div class="columns">
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-briefcase"></i></span>
                        <p class="heading">Marketing</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>

            </div>
            <div class="columns">
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-briefcase"></i></span>
                        <p class="heading">Marketing</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>

            </div>
        </div>
    </section>
    <!-- Services Start-->

    <!-- Ideas Start-->
    <section class="s-ideas section-bg-gray">
        <div class="container">
            <div class="section-heading">
                <h2 class="title">Waxom is Realization of your Ideas.</h2>
                <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            </div>
            <div class="columns columns-container">
                <div class="column column-item">
                    <div class="item item-img"><img src="/app/img/solution.png" alt=""></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Ideas End-->

    <!-- Info container  start-->
    <section class="s-info">
        <div class="container">
            <div class="columns">
                <div class="column  is-6 is-left">
                    <article>
                        <div class="heading">

                            <h2 class="title">About our company</h2>
                        </div>
                        <div class="content">
                            <p>Curabitur accumsan turpis pharetra <strong>augue tincidunt</strong> blandit. Quisque
                                condimentum maximus mi, sit amet commodo arcu rutrum id. Proin pretium urna vel cursus
                                venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec
                                at
                                dignissim dui. Ut et neque nisl.</p>
                            <p>Curabitur accumsan turpis pharetra <strong>augue tincidunt</strong> blandit. Quisque
                                condimentum maximus mi, sit amet commodo arcu rutrum id. Proin pretium urna vel cursus
                                venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec
                                </p>
                        </div>
                    </article>
                </div>
                <div class="column  is-6 is-right">
                    <div class="heading">
                        <div class="title">Our skills</div>
                    </div>
                    <div class="container-progress">
                        <progress class="progress is-large is-success" value="15" max="100">15%</progress>
                        <progress class="progress is-large is-success" value="30" max="100">30%</progress>
                        <progress class="progress is-large is-success" value="45" max="100">45%</progress>
                        <progress class="progress is-large is-success" value="60" max="100">60%</progress>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Info container  end-->


    <!-- SLICK Posts Start-->
    <section class="s-posts blog">
        <div class="container">
            <h2 class="title">Recent Posts</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="wrapper-carousel">

                <div class="columns is-1 slick-posts">
                    <div class="column is-4">
                        <article class="article">
                            <div class="">
                                <div class="list-posts">
                                    <div class="card card__border">
                                        <div class="card-image">
                                            <figure class="image is-4by3 is-marginless">
                                                <img src="/app/img/photo-8.png" alt="Placeholder image">
                                            </figure>
                                            <div class="card-lable">
                                                <span class="tag lable is-black label-number">14</span>
                                                <br>
                                                <span class="tag lable is-black label-month">Fev</span></div>


                                        </div>
                                        <div class="card-content">
                                            <div class="media">
                                                <div class="media-content">
                                                    <span class="title is-4">John Smith</span>

                                                </div>
                                            </div>
                                            <div class="content col-info">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                    Phasellus nec iaculis mauris
                                                </p>
                                                <div class="row-btn">
                                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                            class="fas fa-caret-right"></i></a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-10.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-5.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- SLICK Posts End-->

    <!-- SLICK CLIENTS Start-->
    <section class="clients">
        <div class="slick-clients">
            <div class="slick-clients__item">
                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    .</p>
                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean
                    .</p>


            </div>
            <div class="slick-clients__item">

                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean222222
                    .</p>
                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean
                    .</p>

            </div>
            <div class="slick-clients__item">

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam aspernatur consectetur
                    cumque dignissimoluptate voluptatem?</p>

            </div>
        </div>
    </section>
    <!-- SLICK CLIENTS end-->
    <!-- Partners Start-->
    <section class="s-partners">
<!--        <div class="img-overlay"></div>-->
        <div class="container">
            <div class="columns row-partners">
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-01.png" alt="logo"></div>
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-02.png" alt="logo1"></div>
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-03.png" alt="logo2"></div>
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-04.png" alt="logo3"></div>
            </div>
        </div>
    </section>
    <!-- Partners End-->
    <section class="s-purchase">
        <div class="container">
            <div class="columns is-1">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">

                    <?php button('Purchase Now','#','btn-cta level-item', '') ?>

                </div>
            </div>
        </div>
    </section>

</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
