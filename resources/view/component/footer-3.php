<!--Footer start-->
<div class="scroll-up">
    <a href="#home" class="scroll text-center text-color-white">
        <i class="fa fa-angle-up"></i>
    </a>
</div>


<footer class="footer-3">
    <div class="footer-wrapper">
        <div class="footer-wrapper-top">
            <div class="footer-widget">
                <div class="container">
                    <div class="columns">
                        <div class="column">
                            <div class="widget-text">
                                <div>
                                    <img src="app/img/logo.png" alt="logo"/>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                                    nibh euismod tincidunt
                                    ut laoreet dolore magna aliquam erat volutpat.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy</p>
                            </div>
                        </div>
                        <div class="column">
                            <div class="heading">
                                <h3>Recent Posts</h3>
                            </div>
                            <div class="inner">
                                <div class="widget-post">
                                    <ul class="widget-post__items">
                                        <li class="widget-post__item">
                                            <a>
                                                <p class="post-item__title">Lorem ipsum dolor</p>
                                                <span href="#" class="post-item_meta">September 08, 2015</span>

                                            </a>
                                        </li>
                                        <li class="widget-post__item">
                                            <a>
                                                <p class="post-item__title">Lorem ipsum dolor</p>
                                                <span href="#" class="post-item_meta">September 08, 2015</span>

                                            </a>
                                        </li>
                                        <li class="widget-post__item">
                                            <a>
                                                <p class="post-item__title">Lorem ipsum dolor</p>
                                                <span href="#" class="post-item_meta">September 08, 2015</span>

                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="column">
                            <div class="heading">
                                <h3>Page</h3>
                            </div>
                            <div class="inner">
                                <div class="widget-menu">
                                    <ul class="widget-menu__items">
                                        <li class="widget-menu__item">
                                            <a href="#">Portfolio</a>
                                        </li>
                                        <li class="widget-menu__item">
                                            <a href="#">About us</a>
                                        </li>
                                        <li class="widget-menu__item">
                                            <a href="#blog">Blog</a>
                                        </li>
                                        <li class="widget-menu__item">
                                            <a href="#">Contact</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="heading">
                                <h3>Contact Info</h3>
                            </div>
                            <div class="inner">
                                <div class="widget-contact">
                                    <div class="widget-contact__items">
                                        <div class="widget-contact__item">
                                            <a  href="#"><i class="fas fa-envelope"></i>Department 98
                                                44-46 Morningside Road
                                                Edinburgh, Scotland</a>
                                        </div>
                                        <div class="widget-contact__item">
                                            <a  href="#"><i class="fas fa-envelope"></i>info@consultplus.com</a>
                                        </div>
                                        <div class="widget-contact__item">
                                            <a  href="#"><i class="fas fa-envelope"></i>+885 6586 5554</a>
                                        </div>
                                        <div class="widget-contact__item">
                                            <a  href="#"><i class="fas fa-envelope"></i>info@consultplus.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-wrapper-bottom">
            <div class="footer-bottom">

                <div class="container">

                    <div class="level">

                        <div class="level-left">

                            <div class="level-item">

                                <ul class="level-item__list">

                                    <li class="level-item__item">

                                        <a href="/" class="level-item__link">Copyright © <?php echo date("Y"); ?>
                                            <b>Waxom</b></a>
                                        |
                                    </li>

                                    <li class="level-item__item">

                                        <a href="#" class="level-item__link">Privacy Policy</a>
                                        |
                                    </li>

                                    <li class="level-item__item">

                                        <a href="#" class="level-item__link">FAQ</a>
                                        |
                                    </li>

                                    <li class="level-item__item">

                                        <a href="#" class="level-item__link">Support</a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <div class="level-right">

                            <div class="level-item">

                                <ul class="level-item__list">

                                    <li class="level-item__item">

                                        <a href="/" class="level-item__link">Designed by <b>ThemeFire</b></a>
                                        |
                                    </li>
                                    <li class="level-item__item">

                                        <a href="/" class="level-item__link">Only on <b>Envato Market</b></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="app/js/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="app/js/jquery.spincrement.js"></script>
<!--<script defer src="//use.fontawesome.com/releases/v5.1.1/js/all.js" integrity="sha384-BtvRZcyfv4r0x/phJt9Y9HhnN5ur1Z+kZbKVgzVBAlQZX4jvAuImlIz+bG7TS00a" crossorigin="anonymous"></script>-->
<script src="//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="//unpkg.com/masonry-layout@4.2.2/dist/masonry.pkgd.min.js"></script>
<script src="//unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<!--<script type="text/javascript" src="/node_modules/bulma-extensions/bulma-accordion/dist/bulma-accordion.min.js"></script>-->
<!--<script type="text/javascript" src="/node_modules/bulma-extensions/bulma-carousel/dist/bulma-carousel.min.js"></script>-->
<script src="/app/js/filter.min.js"></script>
<script src="/app/lib/slick-1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.portfolios').filterData({
            aspectRatio: '8:5',
            nOfRow : 3,
            itemDistance : 0
        });
        $('.portfolio-controllers button').on('click',function(){
            $('.portfolio-controllers button').removeClass('active-work');
            $(this).addClass('active-work');
        });
    });
</script>
<script src="app/js/script-jq.js"></script>
<script src="app/js/script.js"></script>

</body>
</html>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">