<!--Footer start-->
<div class="scroll-up">
    <a href="#home" class="scroll text-center text-color-white">
        <i class="fa fa-angle-up"></i>
    </a>
</div>


<footer>
    <div class="footer-wrapper">
        <div class="footer-wrapper-top">
            <div class="footer-top">
                <div class="container">
                    <div class="columns">
                        <div class="column col-info">

                            <div>

                                <div class="row-brand">

                                    <a class="row-drand__link" href="index.php"><img class="row-drand__img"
                                                                                     src="app/img/logo.png" alt="logo"/></a>
                                </div>

                                <p class="row-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                                    nibh euismod tincidunt
                                    ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                                    exerci
                                    suscipit lobortis claritatem.</p>
                                <div class="row-btn">

                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="column col-post">
                            <h3>Recent Posts</h3>
                            <ul class="column-wrap">
                                <li><a class="row-post">
                                        <span href="#" class="row-post__data">September 08, 2015</span>
                                        <p class="row-pos__descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
                                    </a></li>
                                <li><a class="row-post">
                                        <span href="#" class="row-post__data">September 08, 2015</span>
                                        <p class="row-pos__descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
                                    </a></li>
                                <li><a class="row-post">
                                        <span href="#" class="row-post__data">September 08, 2015</span>
                                        <p class="row-pos__descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
                                    </a></li>

                            </ul>
                        </div>
                        <div class="column col-twitter">

                            <h3>Our Twitter</h3>

                            <ul class="column-wrap">
                                <li><a class="row-post">
                                        <p class="row-pos__descr"><span class="color-blue">@waxom </span>Cum soluta nobis
                                            eleifend option congue nihil imperdiet doming</p>
                                        <span href="#" class="row-post__time">3 mins ago</span>
                                    </a></li>
                                <li><a class="row-post">
                                        <p class="row-pos__descr">Mirum est <span class="color-blue">#envato</span> notare quam
                                            littera gothica, quam nunc putamus parum anteposuerit litterarum</p>
                                        <span href="#" class="row-post__time">3 mins ago</span>
                                    </a></li>
                                <li><a class="row-post">
                                        <p class="row-pos__descr">Soluta nobis option <span
                                                    class="color-gray">bit.ly/1Hniso7</span></p>
                                        <span href="#" class="row-post__time">3 mins ago</span>
                                    </a></li>


                            </ul>

                        </div>

                        <div class="column col-widget">

                            <h3>Dribble Widget</h3>

                            <div class="column-wrap row-widget">
                                <div>

                                    <div class="row-widget__project">

                                        <div class="project-item"><img class="project-item__img" src="app/img/f-ph.png" alt="#">
                                        </div>
                                        <div class="project-item"><img class="project-item__img" src="app/img/f-ph-2.png"
                                                                       alt="#"></div>
                                        <div class="project-item"><img class="project-item__img" src="app/img/f-ph-3.png"
                                                                       alt="#"></div>
                                        <div class="project-item"><img class="project-item__img" src="app/img/photo-fotter.png"
                                                                       alt="#"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="footer-wrapper-bottom">
            <div class="footer-bottom">

                <div class="container">

                    <div class="level">

                        <div class="level-left">

                            <div class="level-item">

                                <ul class="level-item__list">

                                    <li class="level-item__item">

                                        <a href="/" class="level-item__link">Copyright © <?php echo date("Y"); ?>
                                            <b>Waxom</b></a>
                                        |
                                    </li>

                                    <li class="level-item__item">

                                        <a href="#" class="level-item__link">Privacy Policy</a>
                                        |
                                    </li>

                                    <li class="level-item__item">

                                        <a href="#" class="level-item__link">FAQ</a>
                                        |
                                    </li>

                                    <li class="level-item__item">

                                        <a href="#" class="level-item__link">Support</a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <div class="level-right">

                            <div class="level-item">

                                <ul class="level-item__list">

                                    <li class="level-item__item">

                                        <a href="/" class="level-item__link">Designed by <b>ThemeFire</b></a>
                                        |
                                    </li>
                                    <li class="level-item__item">

                                        <a href="/" class="level-item__link">Only on <b>Envato Market</b></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


</footer>
<script src="app/js/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="app/js/jquery.spincrement.js"></script>
<!--<script defer src="//use.fontawesome.com/releases/v5.1.1/js/all.js" integrity="sha384-BtvRZcyfv4r0x/phJt9Y9HhnN5ur1Z+kZbKVgzVBAlQZX4jvAuImlIz+bG7TS00a" crossorigin="anonymous"></script>-->
<script src="//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="/node_modules/bulma-carousel/dist/js/bulma-carousel.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="//unpkg.com/masonry-layout@4.2.2/dist/masonry.pkgd.min.js"></script>
<script src="//unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="/app/js/filter.min.js"></script>



<script src="/app/lib/slick-1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.portfolios').filterData({
            aspectRatio: '8:5',
            nOfRow : 3,
            itemDistance : 0
        });
        $('.portfolio-controllers button').on('click',function(){
            $('.portfolio-controllers button').removeClass('active-work');
            $(this).addClass('active-work');
        });
    });
</script>
<script src="app/js/script-jq.js"></script>
<script src="app/js/script.js"></script>

</body>
</html>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">