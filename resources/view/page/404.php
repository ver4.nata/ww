<?php include PATCH . "resources/view/component/header-2.html"; ?>


<!-- Main container start -->
<main class="main-content page-404">
    <section class="breadcrumbs-full">
        <div class="container">
        </div>
    </section>
   <section class="content-404">
        <p class="title title-404">404</p>
        <p class="title">Oops! Page Not Available.</p>
        <p class="text-style" >Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
        <p class="text-style" >Mabe try a search?</p>
   </section>
    <section class="search">
        <div class="container"><form class="form">
            <div class="field has-addons">
                <div class="control is-expanded has-icons-right">
                    <input class="input is-large" type="text" placeholder="Search...">
                    <span class="icon is-right">
                        <i class="fas fa-search fa-sm"></i>
                    </span>
                </div>
            </div>
        </form></div>
    </section>


</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
