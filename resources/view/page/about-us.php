<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content about-us">

    <!-- Breadcrumbs container  start-->
    <section class="breadcrumbs breadcrumbs-full breadcrumbs-bg__img">
        <div class="container">
            <div class="wrapper-content text-centered">
                <h1 class="title title-page">About us</h1>
            </div>
        </div>
    </section>
    <!-- Breadcrumbs container  end-->

    <!-- Excellent container  start-->
    <section class="s-excellent">
        <div class="container">
            <div class="columns">
                <div class="column is-6">
                    <figure class="image">
                        <img src="/app/img/who-we-are.jpg">
                    </figure>
                </div>
                <div class="column is-6 column-gap">
                    <h2 class="title">Whu We Are?</h2>
                    <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                    <ul>
                        <li>Nam liber tempor cum soluta nobis eleifend option;</li>
                        <li>Option congue nihil imperdiet doming id quod mazim;</li>
                        <li>Eodem modo typi, qui nunc nobis videntur parum futurum;</li>
                        <li>Investigationes demonstraverunt lectores</li>
                    </ul>
                </div>
            </div>


        </div>
    </section>
    <!-- Excellent container  end-->

    <!-- Info container  start-->
    <section class="s-info">
        <div class="container">
            <div class="columns">
                <div class="column  is-6 is-left">
                    <div class="heading">
                        <div class="title">What we do</div>
                    </div>
                    <div class="container-tab">
                        <div id="custom-tabs" class="custom-tabs">
                            <ul class="tab">
                                <li><a class="tablinks" href="#tabs-1">Our Mission</a></li>
                                <li><a class="tablinks" href="#tabs-2">Our Technolog</a></li>
                                <li><a class="tablinks" href="#tabs-3">Our Skills</a></li>
                            </ul>
                            <div id="tabs-1" class="tabcontent">
                                <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
                            </div>
                            <div id="tabs-2" class="tabcontent">
                                <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
                            </div>
                            <div id="tabs-3" class="tabcontent">
                                <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
                                <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column  is-6 is-right">
                    <div class="heading">
                        <div class="title">Our skills</div>
                    </div>
                    <div class="container-progress">
                        <progress class="progress is-large is-success" value="15" max="100">15%</progress>
                        <progress class="progress is-large is-success" value="30" max="100">30%</progress>
                        <progress class="progress is-large is-success" value="45" max="100">45%</progress>
                        <progress class="progress is-large is-success" value="60" max="100">60%</progress>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Info container  end-->

    <!-- Counters container  start-->
    <section class="s-counters counters-bg__img" id="counts">
        <div class="container">

            <nav class="level is-mobile">

                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="fas fa-users"></i></span>
                        <p class="spincrement">3,456</p>
                        <p class="heading">Satisfied Clients</p>
                        <p class="border"></p>
                    </div>

                </div>

                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-coffee"></i></span>
                        <p class="spincrement">123</p>
                        <p class="heading">Cups of coffee</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="far fa-comment"></i></span>
                        <p class="spincrement">456</p>
                        <p class="heading">Blog posts</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="far fa-heart"></i></span>
                        <p class="spincrement">789</p>
                        <p class="heading">Likes</p>
                        <p class="border"></p>
                    </div>
                </div>

                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-concierge-bell"></i></span>
                        <p class="spincrement">900</p>
                        <p class="heading">We launched</p>
                        <p class="border"></p>
                    </div>
                </div>

            </nav>
        </div>
    </section>
    <!-- Counters container  end-->

    <!-- Services container  start-->
    <section class="s-services">
        <div class="container">
            <h2 class="title has-text-centered">Our Services</h2>
            <p class="sub-title has-text-centered">Nam liber tempor cum soluta nobis eleifend option congue. Don't Forget to Rate the Template. Thanks so much!</p>
            <div class="columns">
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div>
            <div class="columns">
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Services container  end-->

    <!-- Team container  start-->
    <section class="s-projects s-team">
        <div class="container">
            <h2 class="title">Our Incredible Team</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium  qui sequitur mutationem consuetudium..</p>

                <div class="columns is-1">
                    <div class="column is-3" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img  src="/app/img/500.jpg" alt="w"/>
                            </figure>
                        </div>
                    </div>
                    <div class="column is-3" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img  src="/app/img/529-min.jpg" alt="w"/>
                            </figure>
                        </div>
                    </div>
                    <div class="column is-3" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img src="/app/img/528-min.jpg" alt="w"/>
                            </figure>
                        </div>
                    </div>
                    <div class="column is-3" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img src="/app/img/501.jpg" alt="w"/>
                            </figure>
                        </div>
                    </div>
                </div>

        </div>
    </section>
    <!-- Team container  end-->

    <!-- Partners container  start-->
    <section class="s-partners">

        <div class="container partners">
            <h2 class="title has-text-centered">Our Clients</h2>
            <p class="sub-title has-text-centered">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab amet beatae consequatur dolorum ea enim esse explicabo facere harum labore laudantium magnam, mollitia nam </p>
            <div class="columns row-partners">
                <div class="column"><img src="/app/img/logo-01.png" alt="logo"></div>
                <div class="column"><img  src="/app/img/logo-02.png" alt="logo1"></div>
                <div class="column"><img  src="/app/img/logo-03.png" alt="logo2"></div>
                <div class="column"><img src="/app/img/logo-04.png" alt="logo3"></div>
            </div>
        </div>
    </section>
    <!-- Partners container  end-->
    <!-- Main container  end-->
</main>


<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
