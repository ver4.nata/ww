<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content about-us2">
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">About us 2</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">About us 2</a></li>
                        <!--                    <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <section class="s-info">
        <div class="container">
            <div class="columns">
                <div class="column  is-6 is-left">
                    <article>
                        <div class="heading">

                            <h2 class="title">About our company</h2>
                        </div>
                        <div class="content">
                            <p>Curabitur accumsan turpis pharetra <strong>augue tincidunt</strong> blandit. Quisque
                                condimentum maximus mi, sit amet commodo arcu rutrum id. Proin pretium urna vel cursus
                                venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec
                                at
                                dignissim dui. Ut et neque nisl.</p>
                            <p>Curabitur accumsan turpis pharetra <strong>augue tincidunt</strong> blandit. Quisque
                                condimentum maximus mi, sit amet commodo arcu rutrum id. Proin pretium urna vel cursus
                                venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec
                                at
                                dignissim dui. Ut et neque nisl.Proin pretium urna vel cursus
                                venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec
                                at
                                dignissim dui. Ut et neque nisl.</p>

                        </div>
                    </article>
                </div>
                <div class="column  is-6 is-right">
                    <figure class="image">
                        <img src="/app/img/our-company.jpg">
                    </figure>

                </div>
            </div>
        </div>
    </section>
    <section class="s-purchase">
        <div class="container">
            <div class="columns">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">

                    <?php button('Purchase Now', '#', 'btn-cta level-item', '') ?>

                </div>
            </div>
        </div>
    </section>
    <section class="s-services">
        <div class="container">
            <h2 class="title has-text-centered">What we do?</h2>
            <p class="sub-title has-text-centered">Nam liber tempor cum soluta nobis eleifend option congue</p>
            <div class="columns">
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div>
            <div class="columns">
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="video">
        <figure>

        </figure>

    </section>
    <section class="s-partners">

        <div class="container">
            <div class="columns row-partners">
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-01.png" alt="logo"></div>
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-02.png" alt="logo1"></div>
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-03.png" alt="logo2"></div>
                <div class="column"><img class="img-filter__invert" src="/app/img/logo-04.png" alt="logo3"></div>
            </div>
        </div>
    </section>
    <!-- Main container  end-->
</main>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
