<?php include PATCH ."resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content">
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">About us 3</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">About us 3</a></li>
                        <!--                    <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <section class="s-info">
        <div class="container">
            <div class="columns">
                <div class="column  is-6 is-left">
                    <div class="heading">
                        <div class="title">What we do</div>
                    </div>
                    <div class="container-tab">

                        <div class="tab">
                            <button class="tablinks" onclick="openCity(event, 'London')">London</button>
                            <button class="tablinks" onclick="openCity(event, 'Paris')">Paris</button>
                            <button class="tablinks" onclick="openCity(event, 'Tokyo')">Tokyo</button>
                        </div>

                        <div id="London" class="tabcontent">
                            <h3>London</h3>
                            <p>London is the capital city of England.</p>
                        </div>

                        <div id="Paris" class="tabcontent">
                            <h3>Paris</h3>
                            <p>Paris is the capital of France.</p>
                        </div>

                        <div id="Tokyo" class="tabcontent">
                            <h3>Tokyo</h3>
                            <p>Tokyo is the capital of Japan.</p>
                        </div>

                    </div>
                </div>
                <div class="column  is-6 is-right">
                    <div class="heading">
                        <div class="title">Our skills</div>
                    </div>
                    <div class="container-progress">
                        <progress class="progress is-large is-danger" value="15" max="100">15%</progress>
                        <progress class="progress is-large" value="30" max="100">30%</progress>
                        <progress class="progress is-large" value="45" max="100">45%</progress>
                        <progress class="progress is-large" value="60" max="100">60%</progress>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="s-counters" id="counts">
        <div class="container">

            <nav class="level is-mobile">

                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="fas fa-users"></i></span>
                        <p class="spincrement">3,456</p>
                        <p class="heading">Satisfied Clients</p>
                        <p class="border"></p>
                    </div>

                </div>

                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-coffee"></i></span>
                        <p class="spincrement">123</p>
                        <p class="heading">Cups of coffee</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="far fa-comment"></i></span>
                        <p class="spincrement">456</p>
                        <p class="heading">Blog posts</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="far fa-heart"></i></span>
                        <p class="spincrement">789</p>
                        <p class="heading">Likes</p>
                        <p class="border"></p>
                    </div>
                </div>

                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-concierge-bell"></i></span>
                        <p class="spincrement">900</p>
                        <p class="heading">We launched</p>
                        <p class="border"></p>
                    </div>
                </div>

            </nav>
        </div>
    </section>
    <section class="s-services">
        <div class="container">
            <h2 class="title has-text-centered">Services</h2>
            <div class="columns">
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div>
            <div class="columns">
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column is-4 has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="s-projects s-team">
        <div class="container">
            <h2 class="title">Our Latest Projects</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="tab" id="tab">

                <div class="columns is-1">
                    <div class="column is-3">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/528-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p  class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-3">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/529-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-3">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/536-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-3">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/536-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="testimonial-carusel">
        <div class="container">
            <div class="heading has-text-centered">
                <h2 class="title">Happy Customers</h2>
                <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt </p>
            </div>
            <div class="customer-corusel">
                <div class="columns is-1">
                    <div class="column is-6">
                        <div class="card-content boxes">

                            <p class="content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus nec iaculis mauris.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus nec iaculis mauris.

                            </p>

                            <div class="media">
                                <div class="media-left">
                                    <figure class="image-avatar image">
                                        <img class="img-circle img-circle__testimon" src="/app/img/529-min.jpg" alt="">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <span class="title is-4">John Smith</span>
                                    <span class="subtitle is-6">@johnsmith</span>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="column is-6">
                        <div class="card-content boxes">

                            <p class="content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus nec iaculis mauris.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus nec iaculis mauris.
                            </p>

                            <div class="media">
                                <div class="media-left">
                                    <figure class="image-avatar image">
                                        <img class="img-circle img-circle__testimon" src="/app/img/528-min.jpg" alt="">
                                    </figure>
                                </div>
                                <p class="media-content">
                                    <span class="title is-4">John Smith</span>
                                    <span class="subtitle is-6">@johnsmith</span>
                                </p>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="s-partners">
        <div class="img-overlay"></div>
        <div class="container">
            <div class="columns row-partners">
                <div class="column"><img src="/app/img/logo-01.png" alt="logo"></div>
                <div class="column"><img src="/app/img/logo-02.png" alt="logo1"></div>
                <div class="column"><img src="/app/img/logo-03.png" alt="logo2"></div>
                <div class="column"><img src="/app/img/logo-04.png" alt="logo3"></div>
            </div>
        </div>
    </section>
</main>
<!-- Main container  end-->
</div>

<?php include PATCH ."resources/view/component/footer.php"; ?>
</html>
