<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content">


</main>
<section class="breadcrumbs-full">
    <div class="container">
        <div class="wrapper-content">
            <p class="title title-page">About us 4</p>
            <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                <ul>
                    <li><a class="breadcrumb-item" href="#">home</a></li>
                    <li><a class="breadcrumb-item" href="#">About us 4</a></li>
                    <!--                    <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                </ul>
            </nav>
        </div>
    </div>
</section>
<section class="s-info">
    <div class="container">
        <div class="columns">
            <div class="column  is-6 is-left">
                <figure class="image">
                    <img src="/app/img/photo-6.png">
                </figure>

            </div>
            <div class="column  is-6 is-right">
                <div class="heading">

                    <div class="title">About our company</div>
                    <div class="content">
                        <p>Curabitur accumsan turpis pharetra <strong>augue tincidunt</strong> blandit. Quisque
                            condimentum maximus mi, sit amet commodo arcu rutrum id. Proin pretium urna vel cursus
                            venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec at
                            dignissim dui. Ut et neque nisl.</p>
                        <p>Curabitur accumsan turpis pharetra <strong>augue tincidunt</strong> blandit. Quisque
                            condimentum maximus mi, sit amet commodo arcu rutrum id. Proin pretium urna vel cursus
                            venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec at
                            dignissim dui. Ut et neque nisl.Proin pretium urna vel cursus
                            venenatis. Suspendisse potenti. Etiam mattis sem rhoncus lacus dapibus facilisis. Donec at
                            dignissim dui. Ut et neque nisl.</p>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="s-info">
    <div class="container">
        <div class="columns">
            <div class="column  is-6 is-left">
                <div class="heading">
                    <div class="title">What we do</div>
                </div>
                <div id="accordion">
                    <button class="accordion">Section 1</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
                    </div>

                    <button class="accordion">Section 2</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
                    </div>

                    <button class="accordion">Section 3</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
                    </div>

                </div>
            </div>
            <div class="column  is-6 is-right">
                <div class="heading">
                    <div class="title">Our skills</div>
                </div>
                <div class="container-progress">
                    <progress class="progress is-large is-danger" value="15" max="100">15%</progress>
                    <progress class="progress is-large" value="30" max="100">30%</progress>
                    <progress class="progress is-large" value="45" max="100">45%</progress>
                    <progress class="progress is-large" value="60" max="100">60%</progress>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="s-services">
    <div class="container">
        <h2 class="title has-text-centered">Services</h2>
        <div class="columns">
            <div class="column is-4 has-text-centered">
                <a>
                    <span class="icon"><i class="fas fa-pen-nib"></i></span>
                    <p class="heading">Web & App Design</p>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                    <p class="border"></p>
                </a>
            </div>
            <div class="column is-4 has-text-centered">
                <a>
                    <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                    <p class="heading">Development</p>
                    <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    <p class="border"></p>
                </a>
            </div>
            <div class="column is-4 has-text-centered">
                <a>
                    <span class="icon"><i class="far fa-sun"></i></span>
                    <p class="heading">Customization</p>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                    <p class="border"></p>
                </a>
            </div>
        </div>
        <div class="columns">
            <div class="column is-4 has-text-centered">
                <a>
                    <span class="icon"><i class="fas fa-pen-nib"></i></span>
                    <p class="heading">Web & App Design</p>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                    <p class="border"></p>
                </a>
            </div>
            <div class="column is-4 has-text-centered">
                <a>
                    <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                    <p class="heading">Development</p>
                    <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    <p class="border"></p>
                </a>
            </div>
            <div class="column is-4 has-text-centered">
                <a>
                    <span class="icon"><i class="far fa-sun"></i></span>
                    <p class="heading">Customization</p>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                    <p class="border"></p>
                </a>
            </div>
        </div>
    </div>
</section>


<section class="s-partners">
    <div class="img-overlay"></div>
    <div class="container">
        <div class="columns row-partners">
            <div class="column"><img src="/app/img/logo-01.png" alt="logo"></div>
            <div class="column"><img src="/app/img/logo-02.png" alt="logo1"></div>
            <div class="column"><img src="/app/img/logo-03.png" alt="logo2"></div>
            <div class="column"><img src="/app/img/logo-04.png" alt="logo3"></div>
        </div>
    </div>
</section>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
