<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content blog">

    <!-- Breadcrumbs container  start-->
    <section class="breadcrumbs breadcrumbs-full breadcrumbs-bg__img">
        <div class="container">
            <div class="wrapper-content text-centered">
                <h1 class="title title-page">Page fullscreen</h1>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Page fullscreen</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <div class="container multitype">
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child ">
                    <div class="card card__border">
                        <div class="card-image">
                            <figure class="image is-2by1 is-marginless">
                                <img src="/app/img/photo-6.png" alt="Placeholder image">
                            </figure>
                            <div class="card-lable">
                                <span class="tag lable is-black label-number">14</span>
                                <br>
                                <span class="tag lable is-black label-month">Fev</span></div>


                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <span class="title is-4">John Smith</span>
                                    <div class="card-content__detalies">
                                        <span class="subtitle is-6">Bu:  <span class="value"> @johnsmith</span></span>
                                        <span> On: <span class="value"><time
                                                        datetime="2016-1-1">February 14, 2015 </time> </span></span>
                                        <span class="subtitle is-6">Comments: <span class="value">3</span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="content col-info">

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a></p>
                                <div class="row-btn">

                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                class="fas fa-caret-right"></i></a>
                                </div>


                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child ">
                    <div class="card card__border">
                        <div class="card-image">
                            <figure class="image is-4by3 is-marginless">
                                <img src="/app/img/photo-8.png" alt="Placeholder image">
                            </figure>
                            <div class="card-lable">
                                <span class="tag lable is-black label-number">14</span>
                                <br>
                                <span class="tag lable is-black label-month">Fev</span></div>


                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <span class="title is-4">John Smith</span>
                                    <div class="card-content__detalies">
                                        <span class="subtitle is-6">Bu:  <span class="value"> @johnsmith</span></span>
                                        <span> On: <span class="value"><time
                                                        datetime="2016-1-1">February 14, 2015 </time> </span></span>
                                        <span class="subtitle is-6">Comments: <span class="value">3</span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="content col-info">

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a></p>
                                <div class="row-btn">

                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                class="fas fa-caret-right"></i></a>
                                </div>


                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">

                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image  is-3by2 is-marginless">
                                    <img src="/app/img/photo-7.png" alt="Placeholder image">
                                </figure>
                                <div class="card-lable">
                                    <span class="tag lable is-black label-number">14</span>
                                    <br>
                                    <span class="tag lable is-black label-month">Fev</span></div>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <span class="title is-4">John Smith</span>
                                        <div class="card-content__detalies">
                                            <span class="subtitle is-6">Bu:  <span
                                                        class="value"> @johnsmith</span></span>
                                            <span> On: <span class="value"><time
                                                            datetime="2016-1-1">February 14, 2015 </time> </span></span>
                                            <span class="subtitle is-6">Comments: <span class="value">3</span></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="content col-info">

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Phasellus nec iaculis mauris
                                        <a href="#">#css</a> <a href="#">#responsive</a></p>
                                    <div class="row-btn">

                                        <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <div class="tile is-ancestor">
            <div class="tile is-vertical is-8">
                <div class="tile">
                    <div class="tile is-parent is-vertical">
                        <article class="tile is-child ">
                            <div class="content">
                                <div class="card card__border">
                                    <div class="card-image">
                                        <figure class="image is-marginless">
                                            <img src="/app/img/island.jpg" alt="Placeholder image">
                                        </figure>
                                        <div class="card-lable">
                                            <span class="tag lable is-black label-number">14</span>
                                            <br>
                                            <span class="tag lable is-black label-month">Fev</span></div>


                                    </div>
                                    <div class="card-content">
                                        <div class="media">
                                            <div class="media-content">
                                                <span class="title is-4">bootom</span>
                                                <div class="card-content__detalies">
                                                    <span class="subtitle is-6">Bu:  <span
                                                                class="value"> @johnsmith</span></span>
                                                    <span> On: <span class="value"><time datetime="2016-1-1">February 14, 2015 </time> </span></span>
                                                    <span class="subtitle is-6">Comments: <span
                                                                class="value">3</span></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="content col-info">

                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Phasellus nec iaculis mauris
                                                <a href="#">#css</a> <a href="#">#responsive</a></p>
                                            <div class="row-btn">

                                                <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                            class="fas fa-caret-right"></i></a>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div>
                    <div class="tile is-parent">
                        <article class="tile is-child">
                            <div class="content">
                                <div class="card card__border">
                                    <div class="card-image">
                                        <figure class="image is-1by1 is-marginless">
                                            <img src="/app/img/photo-10.png" alt="Placeholder image">
                                        </figure>
                                        <div class="card-lable">
                                            <span class="tag lable is-black label-number">14</span>
                                            <br>
                                            <span class="tag lable is-black label-month">Fev</span></div>


                                    </div>
                                    <div class="card-content">
                                        <div class="media">
                                            <div class="media-content">
                                                <span class="title is-4">John Smith</span>
                                                <div class="card-content__detalies">
                                                    <span class="subtitle is-6">Bu:  <span
                                                                class="value"> @johnsmith</span></span>
                                                    <span> On: <span class="value"><time datetime="2016-1-1">February 14, 2015 </time> </span></span>
                                                    <span class="subtitle is-6">Comments: <span
                                                                class="value">3</span></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="content col-info">

                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Phasellus nec iaculis mauris
                                                <a href="#">#css</a> <a href="#">#responsive</a></p>
                                            <div class="row-btn">

                                                <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                            class="fas fa-caret-right"></i></a>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-2by1 is-marginless">
                                    <img src="/app/img/photo-7.png" alt="Placeholder image">
                                </figure>
                                <div class="card-lable">
                                    <span class="tag lable is-black label-number">14</span>
                                    <br>
                                    <span class="tag lable is-black label-month">Fev</span></div>


                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <span class="title is-4">John Smith</span>
                                        <div class="card-content__detalies">
                                            <span class="subtitle is-6">Bu:  <span
                                                        class="value"> @johnsmith</span></span>
                                            <span> On: <span class="value"><time
                                                            datetime="2016-1-1">February 14, 2015 </time> </span></span>
                                            <span class="subtitle is-6">Comments: <span class="value">3</span></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="content col-info">

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Phasellus nec iaculis mauris
                                        <a href="#">#css</a> <a href="#">#responsive</a></p>
                                    <div class="row-btn">

                                        <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <section class="s-pagination">
            <nav class="pagination" role="navigation" aria-label="pagination">
                <a class="pagination-previous" title="This is the first page" disabled>Previous</a>
                <a class="pagination-next">Next page</a>
                <ul class="pagination-list style-none">
                    <li>
                        <a class="pagination-link is-current" aria-label="Page 1" aria-current="page">1</a>
                    </li>
                    <li>
                        <a class="pagination-link" aria-label="Goto page 2">2</a>
                    </li>
                    <li>
                        <a class="pagination-link" aria-label="Goto page 3">-></a>
                    </li>
                </ul>
            </nav>
        </section>

    </div>
    <!-- Breadcrumbs container  end-->

    <!-- Excellent container  start-->

    <!-- Excellent container  end-->

    <!-- Info container  start-->

    <!-- Info container  end-->
    <!-- Counters container  start-->

    <!-- Counters container  end-->

    <!-- Services container  start-->

    <!-- Services container  end-->

    <!-- Team container  start-->

    <!-- Team container  end-->

    <!-- Partners container  start-->

    <!-- Partners container  end-->
    <!-- Main container  end-->
</main>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
