<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content cart">
    <!--  container Breadcrumbs  start-->
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">Cart</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">Home</a></li>
                        <li><a class="breadcrumb-item" href="#">Shop</a></li>
                        <li><a class="breadcrumb-item" href="#">Cart</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <!--  container Breadcrumbs  end-->

    <!--  container Alert  start-->
    <div class="alert alert-danger"></div>
    <!--  container Alert end-->
    <!--  container content Cart  start-->
    <div class="container container-cart">
        <div class="columns">
            <div class="column content-cart">
                <form action="#" class="form">
                    <div class="inner-wrapper">
                        <div class="columns is-multiline table-cart__border">
                            <div class="column is-1 is-narrow">
                                <p class="bd-notification is-primary">

                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <p class="bd-notification is-primary">

                                </p>
                            </div>

                            <div class="column is-3 is-narrow">
                                <p class="sub-title">
                                    Product
                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <p class="sub-title">
                                    Price
                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <p class="sub-title"> Quantity
                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <p class="sub-title">Total</p>
                            </div>

                            <div class="column is-1 is-narrow">
                                <p class="X">
                                    <a class="tag  is-delete" id="tag-del"></a>
                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <figure class="image is-32x32">
                                    <img src="https://bulma.io/images/placeholders/256x256.png" alt="product">
                                </figure>
                            </div>
                            <div class="column is-3 is-narrow">
                                <p class="name">
                                    Happy Nihgt
                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <p class="price">
                                    $18.80
                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <p class="quantity btn-block input-group">
                                    <input type="text" value="1" class="form-control" maxlength="3">
                                </p>
                            </div>
                            <div class="column is-2 is-narrow">
                                <p class="total">
                                    $18.80
                                </p>
                            </div>
                            <div class="column">
                                <div class="content-wrapper">
                                    <div class="content-left">
                                        <button class="btn btn-focus">Coupon cc</button>
                                        <button class="btn-cta btn-focus">apply coupon</button>
                                    </div>
                                    <button class="btn-cta btn-focus">update cart</button>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="columns">
                        <div class="column">

                            <span class="title column-title">You may be interested in ..</span>

                            <div class="columns">

                                <div class="column">

                                    <div class="card">
                                        <div class="card-image">
                                            <figure class="image is-1by1">
                                                <img src="/app/img/ninja-silhouette.jpg" alt="ninja-silhouette">
                                            </figure>
                                        </div>
                                        <div class="card-content has-background-white-ter">
                                            <div class="media">
                                                <div class="media-content">
                                                    <p class="title is-4">John Smith</p>
                                                </div>
                                            </div>

                                            <div class="content">
                                                <span class="price">$19.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="column ">

                                    <div class="card">
                                        <div class="card-image">
                                            <figure class="image is-1by1">
                                                <img src="/app/img/woo-ninja.jpg" alt="woo-ninja">
                                            </figure>
                                        </div>
                                        <div class="card-content has-background-white-ter">
                                            <div class="media">

                                                <div class="media-content">
                                                    <p class="title is-4">John Smith</p>

                                                </div>
                                            </div>

                                            <div class="content">
                                                <span class="price">$30.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column table-total">
                            <span class="title column-title">Card Totals</span>
                            <div class="columns is-multiline">
                                <div class="column">
                                    <div class="is-flex align-sp-between table-cart__border">
                                        <div class="text-right is-one-quarter"><strong>Sub-Total:</strong></div>
                                        <div class="text-left is-one-quarter">$326.00</div>
                                    </div>
                                    <div class="is-flex align-sp-between table-cart__border">
                                        <div class="text-right is-one-quarter"><strong>Total:</strong></div>
                                        <div class="text-left is-one-quarter">$326.00</div>
                                    </div>
                                    <div class="mb-20 mt-20">
                                        <a href="#" class="btn-cta btn-checkout btn-focus">proceed to Checkout</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!--  container content Cart  end-->


</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
