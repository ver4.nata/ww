<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content">
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">Contact 2</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">Bulma</a></li>
                        <li><a class="breadcrumb-item" href="#">Contact 2</a></li>
                        <!-- <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <section class="location">
        <div class="container">
            <div class="columns">
                <div class="column is-left">
                    <div class="heading">
                        <div class="title">Our Location</div>
                    </div>
                    <div class="iframe-container map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m22!1m8!1m3!1d5080.039136006351!2d30.4903354!3d50.4593603!3m2!1i1024!2i768!4f13.1!4m11!3e2!4m5!1s0x40d4ce7976bee27b%3A0x1d3e315c9ebba01c!2z0JvRg9C60YzRj9C90L7QstGB0LrQsNGPLCDQmtC40LXQsiwg0LPQvtGA0L7QtCDQmtC40LXQsiwg0KPQutGA0LDQuNC90LA!3m2!1d50.462416999999995!2d30.4818027!4m3!3m2!1d50.459352599999995!2d30.4965838!5e0!3m2!1sru!2sua!4v1532946964013"
                                width="600" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="column is-right">
                    <div class="heading">
                        <div class="title">Leave a message</div>
                    </div>
                    <form action="#">

                        <div class="field">
                            <label class="label">Your Name(required)</label>
                            <div class="control">
                                <input class="input is-medium" type="text" placeholder="Name input">
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Your Email(required)</label>
                            <div class="control">
                                <input class="input is-medium" type="email" placeholder="Email input" value="">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Subject</label>
                            <div class="control">
                                <input class="input is-medium" type="text" placeholder="" value="">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Message</label>
                            <div class="control">
                                <textarea class="textarea" placeholder="Textarea" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <button class="btn-cta">send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
