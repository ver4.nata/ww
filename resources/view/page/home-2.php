<?php include PATCH . "resources/view/component/header.html"; ?>

<!-- Main container start -->
<main class="main-content home-2">
    <!-- Carousel HOME-2 Start-->
    <section class="s-carousel">

        <div class="slick-carousel">
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fan.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                        <?php button('Registration', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Carousel HOME-2 end-->

    <!-- Excellent Start-->
    <section class="s-excellent">
        <div class="container">

            <div class="columns">
                <div class="column is-6">
                    <img src="/app/img/iphone-in-hand.png" alt="iphone">
                </div>
                <div class="column is-6">
                    <h2 class="title">
                        About ua
                    </h2>

                    <p>Lorem ipsum dolor sit amet um exercitationem fugit itaque minus
                        molestias necessitatibus officiis optio perferendis possimus provident quisquam repudiandae sint
                        temporibus ullam, veniam! A adipisci asperiores assumenda eligendi ex illo incidunt laudantium
                        molestias nulla odit omnis optio quidem ratione, reprehenderit, sequi sit vel voluptas?</p>
                    <div>
                        <?php button('lowed', '#', 'btn-cta btn-cta__bege', '') ?>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <!-- Excellent end-->

    <!-- Video Start-->
    <section class="hero is-fullheight"></section>
    <!-- Video End-->

    <!-- Projects masonry Start-->
    <section class="s-projects work s-team masonry">
        <div class="container">

            <h2 class="title">Our Latest Projects</h2>

            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>

            <div class="tab portfolio-controllers-container" id="tab">

                <div class="tab-navigation portfolio-controllers wow fadeLeft" data-wow-duration="1s"
                     data-wow-delay=".1s">
                    <button type="button" class="filter-btn active-work" data-filter="all">All</button>
                    <button type="button" class="filter-btn" data-filter=".web-design">Web Design</button>
                    <button type="button" class="filter-btn" data-filter=".web-development">Web Development</button>
                    <button type="button" class="filter-btn" data-filter=".graphics">Graphics</button>
                    <button type="button" class="filter-btn" data-filter=".wordpress">Wordpress</button>
                </div>
            </div>
            <div class="columns is-1">
                <div class="column is-4" id="tabAll">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-1.png" alt=""/>
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="column is-4 web-design" id="webDesign">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-2.png" alt="">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="column is-4 wordpress" id="wordpress">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-4.png" alt="">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="columns is-1">
                <div class="column is-4 graphics">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-5.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-4 graphics">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-4 graphics">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <?php button('Load More', '#', 'btn-cta btn-cta__bege', '') ?>
    </section>
    <!-- Projects masonry End-->

    <!-- Counts Start-->
    <section class="s-counters counters-bg__img" id="counts">
        <div class="container">
            <nav class="level is-mobile">
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-users"></i></span>
                        <p class="spincrement">3,456</p>
                        <p class="heading">Satisfied Clients</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-coffee"></i></span>
                        <p class="spincrement">123</p>
                        <p class="heading">Cups of coffee</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="far fa-comment"></i></span>
                        <p class="spincrement">456</p>
                        <p class="heading">Blog posts</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="far fa-heart"></i></span>
                        <p class="spincrement">789</p>
                        <p class="heading">Likes</p>
                        <p class="border"></p>
                    </div>
                </div>
            </nav>
        </div>
    </section>
    <!-- Counts End-->

    <!-- SLICK Posts Start-->
    <section class="s-posts blog">
        <div class="container">
            <h2 class="title">Recent Posts</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="wrapper-carousel">

                <div class="columns is-1 slick-posts">
                    <div class="column is-4">
                        <article class="article">
                            <div class="">
                                <div class="list-posts">
                                    <div class="card card__border">
                                        <div class="card-image">
                                            <figure class="image is-4by3 is-marginless">
                                                <img src="/app/img/photo-8.png" alt="Placeholder image">
                                            </figure>
                                            <div class="card-lable">
                                                <span class="tag lable is-black label-number">14</span>
                                                <br>
                                                <span class="tag lable is-black label-month">Fev</span></div>


                                        </div>
                                        <div class="card-content">
                                            <div class="media">
                                                <div class="media-content">
                                                    <span class="title is-4">John Smith</span>

                                                </div>
                                            </div>
                                            <div class="content col-info">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                    Phasellus nec iaculis mauris
                                                </p>
                                                <div class="row-btn">
                                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                                class="fas fa-caret-right"></i></a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-10.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-5.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- SLICK Posts End-->

    <!-- SLICK CLIENTS Start-->
    <section class="clients">
        <div class="slick-clients">
            <div class="slick-clients__item">


                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    .</p>
                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean
                    .</p>


            </div>
            <div class="slick-clients__item">

                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean222222
                    .</p>
                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean
                    .</p>

            </div>
            <div class="slick-clients__item">

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam aspernatur consectetur
                    cumque dignissimoluptate voluptatem?</p>

            </div>
        </div>
    </section>
    <!-- SLICK CLIENTS end-->

    <!-- SLICK TEAM Start-->
    <section class="team s-team">
        <h2 class="title">Our TEAM</h2>
        <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas
            est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
        <div class="container">
            <div class="columns slick-team">
                <div class="column is-4">
                    <div class="boxes">
                        <figure class="image-avatar">
                            <img src="/app/img/528-min.jpg" alt=""/>

                        </figure>
                        <div class="team-content">
                            <div class="content">
                                <p class="full-name"><strong>John Smith</strong></p>
                                <p class="sub-title">Developer</p>
                                <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius.</p>
                                <nav class="level is-mobile is-center">
                                    <div class="level-is_center">
                                        <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="boxes">
                        <figure class="image-avatar">
                            <img src="/app/img/529-min.jpg" alt=""/>

                        </figure>
                        <div class="team-content">
                            <div class="content">
                                <p class="full-name"><strong>John Smith</strong></p>
                                <p class="sub-title">Developer</p>
                                <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius.</p>
                                <nav class="level is-mobile is-center">
                                    <div class="level-is_center">
                                        <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="boxes">
                        <figure class="image-avatar">
                            <img src="/app/img/536-min.jpg" alt=""/>

                        </figure>
                        <div class="team-content">
                            <div class="content">
                                <p class="full-name"><strong>John Smith</strong></p>
                                <p class="sub-title">Developer</p>
                                <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius.</p>
                                <nav class="level is-mobile is-center">
                                    <div class="level-is_center">
                                        <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="boxes">
                        <figure class="image-avatar">
                            <img src="/app/img/528-min.jpg" alt=""/>

                        </figure>
                        <div class="team-content">
                            <div class="content">
                                <p class="full-name"><strong>John Smith</strong></p>
                                <p class="sub-title">Developer</p>
                                <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius.</p>
                                <nav class="level is-mobile is-center">
                                    <div class="level-is_center">
                                        <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="boxes">
                        <figure class="image-avatar">
                            <img src="/app/img/528-min.jpg" alt=""/>

                        </figure>
                        <div class="team-content">
                            <div class="content">
                                <p class="full-name"><strong>John Smith</strong></p>
                                <p class="sub-title">Developer</p>
                                <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius.</p>
                                <nav class="level is-mobile is-center">
                                    <div class="level-is_center">
                                        <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="boxes">
                        <figure class="image-avatar">
                            <img src="/app/img/528-min.jpg" alt=""/>

                        </figure>
                        <div class="team-content">
                            <div class="content">
                                <p class="full-name"><strong>John Smith</strong></p>
                                <p class="sub-title">Developer</p>
                                <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius.</p>
                                <nav class="level is-mobile is-center">
                                    <div class="level-is_center">
                                        <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                        <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                        </a>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- SLICK TEAM end-->

    <section class="s-purchase">
        <div class="container">
            <div class="columns">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">
                    <?php button('Purchase Now', '#', 'btn-cta btn-cta__bege', '') ?>

                </div>
            </div>
        </div>
    </section>
    <section class="location">
        <div class="container">

            <h2 class="title">Our Location</h2>
            <p class="sub-title">Keep and tpought</p>
            <div class="columns">
                <div class="column is-left">

                    <div class="iframe-container map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m22!1m8!1m3!1d5080.039136006351!2d30.4903354!3d50.4593603!3m2!1i1024!2i768!4f13.1!4m11!3e2!4m5!1s0x40d4ce7976bee27b%3A0x1d3e315c9ebba01c!2z0JvRg9C60YzRj9C90L7QstGB0LrQsNGPLCDQmtC40LXQsiwg0LPQvtGA0L7QtCDQmtC40LXQsiwg0KPQutGA0LDQuNC90LA!3m2!1d50.462416999999995!2d30.4818027!4m3!3m2!1d50.459352599999995!2d30.4965838!5e0!3m2!1sru!2sua!4v1532946964013"
                                width="600" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="column is-right">

                    <form action="#">

                        <div class="field">
                            <label class="label">Your Name(required)</label>
                            <div class="control">
                                <input class="input is-medium" type="text" placeholder="Name input">
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Your Email(required)</label>
                            <div class="control">
                                <input class="input is-medium" type="email" placeholder="Email input" value="">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Subject</label>
                            <div class="control">
                                <input class="input is-medium" type="text" placeholder="" value="">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Message</label>
                            <div class="control">
                                <textarea class="textarea" placeholder="Textarea" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <?php button('send', '#', 'btn-cta btn-cta__bege', '') ?>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
