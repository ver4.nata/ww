<?php include PATCH . "resources/view/component/header.html"; ?>

<!-- Main container start -->
<main class="main-content home-2">
    <!-- Carousel HOME-2 Start-->
    <section class="s-carousel">

        <div class="slick-carousel">
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fan.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                        <?php button('Registration', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Carousel HOME-2 end-->

    <!-- Services Start-->
    <section class="s-services">
        <div class="container">
            <div class="columns">
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-briefcase"></i></span>
                        <p class="heading">Marketing</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div
        </div>
    </section>
    <!-- Services End-->
    <!-- Ideas Start-->
    <section class="s-ideas">
        <div class="container"> <h2 class="title">Waxom is Realization of your Ideas.</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="wrapper-img">
                <figure class="browser-img browser-right">
                    <img src="/app/img/browser-right.png" alt="browser-right">
                </figure>
                <figure class="browser-img browser-center">
                    <img src="/app/img/browser-center.png" alt="browser-center">
                </figure>
                <figure class="browser-img browser-left">
                    <img src="/app/img/browser-left.png" alt="browser-left">
                </figure>
            </div></div>
    </section>
    <!-- Ideas End-->

    <!-- Purchase Start-->
    <section class="s-purchase">
        <div class="container">
            <div class="columns">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">
                    <?php button('Purchase Now', '#', 'btn-cta btn-cta__bege', '') ?>

                </div>
            </div>
        </div>
    </section>
    <!-- Purchase End-->

    <!-- Projects masonry Start-->
    <section class="s-projects work s-team masonry">
        <div class="container">

            <h2 class="title">Our Latest Projects</h2>

            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>

            <div class="tab portfolio-controllers-container" id="tab">

                <div class="tab-navigation portfolio-controllers wow fadeLeft" data-wow-duration="1s"
                     data-wow-delay=".1s">
                    <button type="button" class="filter-btn active-work" data-filter="all">All</button>
                    <button type="button" class="filter-btn" data-filter=".web-design">Web Design</button>
                    <button type="button" class="filter-btn" data-filter=".web-development">Web Development</button>
                    <button type="button" class="filter-btn" data-filter=".graphics">Graphics</button>
                    <button type="button" class="filter-btn" data-filter=".wordpress">Wordpress</button>
                </div>
            </div>
            <div class="columns is-1">
                <div class="column is-4" id="tabAll">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-1.png" alt=""/>
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="column is-4 web-design" id="webDesign">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-2.png" alt="">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="column is-4 wordpress" id="wordpress">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-4.png" alt="">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="columns is-1">
                <div class="column is-4 graphics">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-5.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-4 graphics">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-4 graphics">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <strong>John Smith</strong>
                                    <br>
                                    Lorem ipsum dolor sit amet.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <?php button('Load More', '#', 'btn-cta btn-cta__bege', '') ?>
    </section>
    <!-- Projects masonry End-->

    <!-- Video Start-->
    <section class="hero is-fullheight"></section>
    <!-- Video End-->

    <!-- Excellent Start-->
    <section class="s-excellent">
        <div class="container">

            <div class="columns">
                <div class="column column-img">
                    <img src="/app/img/iphone-in-hand.png" alt="iphone">
                </div>
                <div class="column column-text">
                    <h2 class="title">Excellent for Mobile Devices.</h2>
                    <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                    <ul>
                        <li>Nam liber tempor cum soluta nobis eleifend option;</li>
                        <li>Option congue nihil imperdiet doming id quod mazim placerat facer;</li>
                        <li>Eodem modo typi, qui nunc nobis videntur parum futurum;</li>
                        <li>Investigationes demonstraverunt lectores</li>
                    </ul>
                </div>
            </div>


        </div>
    </section>
    <!-- Excellent end-->

    <!-- Counts Start-->
    <section class="s-counters counters-bg__img" id="counts">
        <div class="container">
            <nav class="level is-mobile">
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-users"></i></span>
                        <p class="spincrement">3,456</p>
                        <p class="heading">Satisfied Clients</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-coffee"></i></span>
                        <p class="spincrement">123</p>
                        <p class="heading">Cups of coffee</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="far fa-comment"></i></span>
                        <p class="spincrement">456</p>
                        <p class="heading">Blog posts</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="far fa-heart"></i></span>
                        <p class="spincrement">789</p>
                        <p class="heading">Likes</p>
                        <p class="border"></p>
                    </div>
                </div>
            </nav>
        </div>
    </section>
    <!-- Counts End-->

    <!-- SLICK Posts Start-->
    <section class="s-posts blog">
        <div class="container">
            <h2 class="title">Recent Posts</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="wrapper-carousel">

                <div class="columns is-1 slick-posts">
                    <div class="column is-4">
                        <article class="article">
                            <div class="">
                                <div class="list-posts">
                                    <div class="card card__border">
                                        <div class="card-image">
                                            <figure class="image is-4by3 is-marginless">
                                                <img src="/app/img/photo-8.png" alt="Placeholder image">
                                            </figure>
                                            <div class="card-lable">
                                                <span class="tag lable is-black label-number">14</span>
                                                <br>
                                                <span class="tag lable is-black label-month">Fev</span></div>


                                        </div>
                                        <div class="card-content">
                                            <div class="media">
                                                <div class="media-content">
                                                    <span class="title is-4">John Smith</span>

                                                </div>
                                            </div>
                                            <div class="content col-info">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                    Phasellus nec iaculis mauris
                                                </p>
                                                <div class="row-btn">
                                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                            class="fas fa-caret-right"></i></a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-10.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-5.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- SLICK Posts End-->

    <!-- Partners Start-->
    <section class="s-partners">
        <div class="img-overlay"></div>
        <div class="container">
            <div class="columns row-partners">
                <div class="column"><img src="/app/img/logo-01.png" alt="logo"></div>
                <div class="column"><img src="/app/img/logo-02.png" alt="logo1"></div>
                <div class="column"><img src="/app/img/logo-03.png" alt="logo2"></div>
                <div class="column"><img src="/app/img/logo-04.png" alt="logo3"></div>
            </div>
        </div>
    </section>
    <!-- Partners End-->

</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
