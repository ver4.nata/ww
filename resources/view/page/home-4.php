<?php include PATCH . "resources/view/component/header.html"; ?>

<!-- Main container start -->
<main class="main-content home">
    <!-- Carousel HOME-2 Start-->
    <section class="s-carousel">

        <div class="slick-carousel">
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fan.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                        <?php button('Registration', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
            <div class="carousel-item">
                <div class="slider-item__img"><img src="/app/img/fog.jpg" alt=""/></div>
                <div class="slider-item__wrapper">
                    <div class="slider-item__title-h1"><h1>we are awesome areative agency</h1></div>
                    <div class="slider-item__content"><p>This is Photoshop's version of Lorem Ipsum. Proin
                            gravida
                            nibh
                            vel velit auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                            nibh
                            id
                            elit.
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                            velit.</p>
                    </div>
                    <div class="slider-item__button">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Carousel HOME-2 end-->
    <!-- Excellent Start-->
    <section class="s-excellent excellent">
        <div class="container">

            <div class="columns excellent-block">
                <div class="column is-6 excellent-block-item item-centered">
                    <img src="/app/img/iphone-in-hand.png" alt="iphone">
                </div>
                <div class="column is-6 excellent-block-item">
                    <h2 class="title">
                        About ua
                    </h2>

                    <p>Lorem ipsum dolor sit amet um exercitationem fugit itaque minus
                        molestias necessitatibus officiis optio perferendis possimus provident quisquam repudiandae sint
                        temporibus ullam, veniam! A adipisci asperiores assumenda eligendi ex illo incidunt laudantium
                        molestias nulla odit omnis optio quidem ratione, reprehenderit, sequi sit vel voluptas?</p>
                    <div>
                        <?php button('lowed', '#', 'btn-cta btn-cta__bege', '') ?>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <!-- Excellent end-->
    <!-- Counts Start-->
    <section class="s-counters counters-bg__img" id="counts">
        <div class="container">
            <nav class="level">
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-users"></i></span>
                        <p class="spincrement">3,456</p>
                        <p class="heading">Satisfied Clients</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-coffee"></i></span>
                        <p class="spincrement">123</p>
                        <p class="heading">Cups of coffee</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="far fa-comment"></i></span>
                        <p class="spincrement">456</p>
                        <p class="heading">Blog posts</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="far fa-heart"></i></span>
                        <p class="spincrement">789</p>
                        <p class="heading">Likes</p>
                        <p class="border"></p>
                    </div>
                </div>
            </nav>
        </div>
    </section>
    <!-- Counts End-->
    <!-- work  masonry Start-->
    <section class="s-projects work s-team masonry">
        <div class="c">
            <h2 class="title">Our Latest Projects</h2>

            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>

            <div class="tab portfolio-controllers-container" id="tab">
                <div class="tab-navigation portfolio-controllers wow fadeLeft" data-wow-duration="1s" data-wow-delay=".1s">
                    <button type="button" class="filter-btn active-work" data-filter="all">All</button>
                    <button type="button" class="filter-btn" data-filter=".web-design">Web Design</button>
                    <button type="button" class="filter-btn" data-filter=".web-development">Web Development</button>
                    <button type="button" class="filter-btn" data-filter=".graphics">Graphics</button>
                    <button type="button" class="filter-btn" data-filter=".wordpress">Wordpress</button>
                </div>
            </div>
        </div>
        <div class="column-wrapper">

            <div class="columns is-1">
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-5.png" alt="p">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>

                        </figure>


                    </div>
                </div>
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>


                    </div>
                </div>
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>


                    </div>
                </div>
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>

                    </div>
                </div>
            </div>
            <div class="columns is-1">
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-5.png" alt="p">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>

                        </figure>


                    </div>
                </div>
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>


                    </div>
                </div>
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>


                    </div>
                </div>
                <div class="column is-3 work-item">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>


                    </div>
                </div>
            </div>
        </div>



    </section>
    <!-- work  masonry end-->

    <!-- SLICK CLIENTS Start-->
    <section class="clients">
        <div class="slick-clients">
            <div class="slick-clients__item">


                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    .</p>
                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean
                    .</p>


            </div>
            <div class="slick-clients__item">

                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean222222
                    .</p>
                <p>This is Photoshop's version of Lorem Ipsum. Proin
                    gravida
                    nibh
                    vel velit auctor aliquet. Aenean
                    .</p>

            </div>
            <div class="slick-clients__item">

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam aspernatur consectetur
                    cumque dignissimoluptate voluptatem?</p>

            </div>
        </div>
    </section>
    <!-- SLICK CLIENTS end-->

    <!-- SLICK Posts Start-->
    <section class="s-posts blog">
        <div class="container">
            <h2 class="title">Recent Posts</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="wrapper-carousel">

                <div class="columns is-1 slick-posts">
                    <div class="column is-4">
                        <article class="article">
                            <div class="">
                                <div class="list-posts">
                                    <div class="card card__border">
                                        <div class="card-image">
                                            <figure class="image is-4by3 is-marginless">
                                                <img src="/app/img/photo-8.png" alt="Placeholder image">
                                            </figure>
                                            <div class="card-lable">
                                                <span class="tag lable is-black label-number">14</span>
                                                <br>
                                                <span class="tag lable is-black label-month">Fev</span></div>


                                        </div>
                                        <div class="card-content">
                                            <div class="media">
                                                <div class="media-content">
                                                    <span class="title is-4">John Smith</span>

                                                </div>
                                            </div>
                                            <div class="content col-info">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                    Phasellus nec iaculis mauris
                                                </p>
                                                <div class="row-btn">
                                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                            class="fas fa-caret-right"></i></a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-10.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-5.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                    class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- SLICK Posts End-->

    <!-- Partners Start-->
    <section class="s-partners">
        <div class="img-overlay"></div>
        <div class="container">
            <div class="columns row-partners partners-list">
                <div class="column is-3 partners-list__item"><img src="/app/img/logo-01.png" alt="logo"></div>
                <div class="column is-3 partners-list__item"><img src="/app/img/logo-02.png" alt="logo1"></div>
                <div class="column is-3 partners-list__item"><img src="/app/img/logo-03.png" alt="logo2"></div>
                <div class="column is-3 partners-list__item"><img src="/app/img/logo-04.png" alt="logo3"></div>
            </div>
        </div>
    </section>
    <!-- Partners End-->

</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
