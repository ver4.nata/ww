<?php include PATCH . "resources/view/component/header.html"; ?>

<!-- Main container start -->
<main class="main-content">
    <section class="s-carousel has-carousel">

        <div class='carousel carousel-animated carousel-animate-slide has-text-centered' data-autoplay="true">

            <div class='carousel-container'>

                <div class='carousel-item has-background is-active'>

                    <div class="img-overlay">
                        <img class="is-background img-overlay" src="/app/img/island.jpg" alt=""
                             width="100%"/>
                    </div>
                    <div class="cont-overlay">
                        <p class="title-t">Unique and Modern Design</p>
                        <h1 class="title">Portfolio PSD Template</h1>
                        <p class="title-b">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet
                            doming
                            id quod mazim placerat facer possim assum.</p>

                        <?php button('Get Started', '#', 'btn-cta', '') ?>

                    </div>
                </div>
            </div>

        </div>
        </div>
    </section>
    <section class="s-services">
        <div class="container">
            <div class="columns">
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-pen-nib"></i></span>
                        <p class="heading">Web & App Design</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fab fa-stack-overflow"></i></span>
                        <p class="heading">Development</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="far fa-sun"></i></span>
                        <p class="heading">Customization</p>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod
                            mazim.</p>
                        <p class="border"></p>
                    </a>
                </div>
                <div class="column has-text-centered">
                    <a>
                        <span class="icon"><i class="fas fa-briefcase"></i></span>
                        <p class="heading">Marketing</p>
                        <p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet.</p>
                        <p class="border"></p>
                    </a>
                </div>
            </div
        </div>
    </section>
    <section class="s-ideas">
        <div class="container">
            <h2 class="title">Waxom is Realization of your Ideas.</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="wrapper-img">
                <figure class="browser-img browser-right">
                    <img src="/app/img/browser-right.png" alt="browser-right">
                </figure>
                <figure class="browser-img browser-center">
                    <img src="/app/img/browser-center.png" alt="browser-center">
                </figure>
                <figure class="browser-img browser-left">
                    <img src="/app/img/browser-left.png" alt="browser-left">
                </figure>
            </div>
        </div>
    </section>
    <section class="s-purchase">
        <div class="container">
            <div class="columns is-1">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">

                    <?php button('Purchase Now', '#', 'btn-cta level-item', '') ?>

                </div>
            </div>
        </div>
    </section>
    <section class="s-projects">
        <div class="container">
            <h2 class="title">Our Latest Projects</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="tab" id="tab">
                <ul class="tab-navigation">
                    <li><a href="#tabAll">All</a></li>
                    <li><a href="#webDesign">Web Design</a></li>
                    <li><a href="#mobileApp">Mobile App</a></li>
                    <li><a href="#illustration">Illustration</a></li>
                    <li><a href="#photography">Photography</a></li>
                </ul>

                <div class="columns is-1">
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image image_zoom">
                                <img src="/app/img/photo-1.png" alt=""/>
                                <div class="boxes-hover">
                                    <div class="icons">
                                        <a><i class="fas fa-search icon-link"></i></a>
                                        <a><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>John Smith</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-4" id="webDesign">
                        <div class="boxes">
                            <figure class="image image_zoom">
                                <img src="/app/img/photo-2.png" alt="">
                                <div class="boxes-hover">
                                    <div class="icons">
                                        <a><i class="fas fa-search icon-link"></i></a>
                                        <a><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>John Smith</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet.
                                    </p>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="column is-4" id="webDesign">
                        <div class="boxes">
                            <figure class="image image_zoom">
                                <img src="/app/img/photo-4.png" alt="">
                                <div class="boxes-hover">
                                    <div class="icons">
                                        <a><i class="fas fa-search icon-link"></i></a>
                                        <a><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>John Smith</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet.
                                    </p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="columns is-1">
                    <div class="column is-4">
                        <div class="boxes">
                            <figure class="image image_zoom">
                                <img src="/app/img/photo-5.png" alt="/">
                                <div class="boxes-hover">
                                    <div class="icons">
                                        <a><i class="fas fa-search icon-link"></i></a>
                                        <a><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>John Smith</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="boxes">
                            <figure class="image image_zoom">
                                <img src="/app/img/photo-6.png" alt="/">
                                <div class="boxes-hover">
                                    <div class="icons">
                                        <a><i class="fas fa-search icon-link"></i></a>
                                        <a><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>John Smith</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="boxes">
                            <figure class="image image_zoom">
                                <img src="/app/img/photo.png" alt="/">
                                <div class="boxes-hover">
                                    <div class="icons">
                                        <a><i class="fas fa-search icon-link"></i></a>
                                        <a><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <strong>John Smith</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>
    <section class="s-video">
        <div id="trailer" class="is_overlay" style="height: 700px">
        </div>
        <p><b>Waxom Video Presentation</b></p><br>
        <p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius, qui sequitur mutationem
            consuetudium.</p>
        <p class="time">03:26</p>
    </section>
    <!-- Excellent Start-->
    <section class="s-excellent">
        <div class="container">
            <div class="columns">
                <div class="column has-text-centered">
                    <img src="/app/img/iphone-in-hand.png" alt="iphone">
                </div>
                <div class="column">
                    <h2 class="title">Excellent for Mobile Devices.</h2>
                    <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                        Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                    <ul>
                        <li>Nam liber tempor cum soluta nobis eleifend option;</li>
                        <li>Option congue nihil imperdiet doming id quod mazim placerat facer;</li>
                        <li>Eodem modo typi, qui nunc nobis videntur parum futurum;</li>
                        <li>Investigationes demonstraverunt lectores</li>
                    </ul>
                </div>
            </div>


        </div>
    </section>
    <!-- Excellent End-->

    <!-- Counters Start-->
    <section class="s-counters counters-bg__img" id="counts">
        <div class="container">
            <nav class="level">
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-users"></i></span>
                        <p class="spincrement">3,456</p>
                        <p class="heading">Satisfied Clients</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-coffee"></i></span>
                        <p class="spincrement">123</p>
                        <p class="heading">Cups of coffee</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">

                    <div>
                        <span class="icon"><i class="far fa-comment"></i></span>
                        <p class="spincrement">456</p>
                        <p class="heading">Blog posts</p>
                        <p class="border"></p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="far fa-heart"></i></span>
                        <p class="spincrement">789</p>
                        <p class="heading">Likes</p>
                        <p class="border"></p>
                    </div>
                </div>

                <div class="level-item has-text-centered">
                    <div>
                        <span class="icon"><i class="fas fa-concierge-bell"></i></span>
                        <p class="spincrement">900</p>
                        <p class="heading">We launched</p>
                        <p class="border"></p>
                    </div>
                </div>

            </nav>
        </div>
    </section>
    <!-- Counters End-->
    <!-- SLICK Posts Start-->
    <section class="s-posts blog">
        <div class="container">
            <h2 class="title">Recent Posts</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="wrapper-carousel">
                <div class="columns slick-posts">
                    <div class="column">
                        <article class="article">
                            <div class="">
                                <div class="list-posts">
                                    <div class="card card__border card-hover">
                                        <div class="card-image">
                                            <figure class="image is-4by3 is-marginless image-hover image_zoom">
                                                <img class="i" src="/app/img/photo-8.png" alt="Placeholder image">
                                            </figure>
                                            <div class="card-lable">
                                                <span class="tag lable is-black label-number">14</span>
                                                <br>
                                                <span class="tag lable is-black label-month">Fev</span></div>
                                        </div>
                                        <div class="card-content">
                                            <div class="media">
                                                <div class="media-content">
                                                    <span class="title is-4">John Smith</span>
                                                </div>
                                            </div>
                                            <div class="content col-info">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                    Phasellus nec iaculis mauris
                                                </p>
                                                <div class="row-btn">
                                                    <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                                class="fas fa-caret-right"></i></a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </div>
                    <div class="column">
                        <div class="list-posts">
                            <div class="card card__border card-hover">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless image-hover image_zoom">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>
                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="list-posts">
                            <div class="card card__border card-hover">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless image-hover image_zoom">
                                        <img src="/app/img/photo-10.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>
                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="list-posts">
                            <div class="card card__border card-hover">
                                <div class="card-image">
                                    <figure class="image is-4by3 is-marginless image-hover image_zoom">
                                        <img src="/app/img/photo-5.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>


                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="list-posts">
                            <div class="card card__border">
                                <div class="card-image card-hover">
                                    <figure class="image is-4by3 is-marginless  image-hover image_zoom">
                                        <img src="/app/img/photo-1.png" alt="Placeholder image">
                                    </figure>
                                    <div class="card-lable">
                                        <span class="tag lable is-black label-number">14</span>
                                        <br>
                                        <span class="tag lable is-black label-month">Fev</span></div>
                                </div>
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <span class="title is-4">John Smith</span>

                                        </div>
                                    </div>
                                    <div class="content col-info">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                        </p>
                                        <div class="row-btn">
                                            <a href="#" class="row-btn__cta"><span>Read More</span><i
                                                        class="fas fa-caret-right"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SLICK Posts End-->
</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
