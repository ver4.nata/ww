<?php include PATCH . "resources/view/component/header-2.html"; ?>


<!-- Main container start -->
<main class="main-content">
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">Portfolio</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Portfolio</a></li>
                        <!--                    <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>

    </section>
    <section class="s-projects work s-team masonry">
        <div class="container">

            <h2 class="title">Our Latest Projects</h2>

            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>

            <div class="tab portfolio-controllers-container" id="tab">

                <div class="tab-navigation portfolio-controllers wow fadeLeft" data-wow-duration="1s" data-wow-delay=".1s">
                    <button type="button" class="filter-btn active-work" data-filter="all">All</button>
                    <button type="button" class="filter-btn" data-filter=".web-design">Web Design</button>
                    <button type="button" class="filter-btn" data-filter=".web-development">Web Development</button>
                    <button type="button" class="filter-btn" data-filter=".graphics">Graphics</button>
                    <button type="button" class="filter-btn" data-filter=".wordpress">Wordpress</button>
                </div>
            </div>
        </div>

        <div class="container">

            <section class="portfolios">
                <div class="portfolio web-design">
                    <figure class="portfolio-image">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/orange-tree.jpg" />
                        <figcaption class="caption">
                            <div class="caption-content">
                                <h3 class="portfolio-item-title text-center sub-title">Creative Design</h3>
                                <ul class="portfolio-link">
                                    <li class="icon-links"><a class="icon" href="#"><i class="fas fa-link icon-fa" aria-hidden="true"></i></a></li>
                                    <li class="icon-links"><a class="icon" href="#"><i class="fas fa-plus icon-fa" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="portfolio web-design">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/submerged.jpg" />
                </div>

                <div class="portfolio web-design">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/submerged.jpg" />
                </div>

                <div class="portfolio web-development">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/look-out.jpg" />
                </div>

                <div class="portfolio wordpress web-design">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/one-world-trade.jpg" />
                </div>

                <div class="portfolio wordpress web-design">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/drizzle.jpg" />
                </div>

                <div class="portfolio wordpress">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/cat-nose.jpg" />
                </div>

                <div class="portfolio wordpress web-design">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/contrail.jpg" />
                </div>

                <div class="portfolio wordpress web-design">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/golden-hour.jpg" />
                </div>

                <div class="portfolio web-development">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/flight-formation.jpg" />
                </div>

                <div class="portfolio web-development">

                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/look-out.jpg" />
                </div>
            </section>

            <div class="more-container text-center">

                <a href="#" class="btn-cta">See More</a>
            </div>

        </div>

    </section>
    <section class="s-purchase bg-wight">
        <div class="container">
            <div class="columns">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">

                    <?php button('Purchase Now', '#', 'btn-cta level-item', '') ?>

                </div>
            </div>
        </div>
    </section>
</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
