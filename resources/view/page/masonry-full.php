<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content">
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">Portfolio</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Portfolio</a></li>
                        <!--                    <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>

    </section>
    <section class="s-projects s-team masonry">
        <div class="container">
            <h2 class="title">Our Latest Projects</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="tab" id="tab">
                <div id="filters-masonry" class="tab-navigation button-group">
                    <button class="button is-checked" data-filter="*">All</button>
                    <button class="button" data-filter=".web-design">Web Design</button>
                    <button class="button" data-filter=".mobile-app">Mobile App</button>
                    <button class="button" data-filter=".illustration">Illustration</button>
                    <button class="button" data-filter=".photography">Photography</button>
                </div>
            </div>

        </div>
        <section class="grid js-masonry">
                <div class="grid-sizer"></div>
                <div class="grid-item element-item web-design" data-category="web-design">
                    <img class="name" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/orange-tree.jpg" />
                </div>
                <div class="grid-item element-item web-design" data-category="web-design">
                    <img class="name" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/submerged.jpg" />
                </div>
                <div class="grid-item element-item mobile-app" data-category="mobile-app">
                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/look-out.jpg" />
                </div>
                <div class="grid-item element-item web-design" data-category="web-design">
                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/one-world-trade.jpg" />
                </div>
                <div class="grid-item element-item illustration" data-category="illustration">
                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/drizzle.jpg" />
                </div>
                <div class="grid-item element-item illustration" data-category="illustration">
                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/cat-nose.jpg" />
                </div>
                <div class="grid-item element-item illustration" data-category="illustration">
                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/contrail.jpg" />
                </div>
                <div class="grid-item element-item illustration" data-category="illustration">
                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/golden-hour.jpg" />
                </div>
                <div class="grid-item element-item mobile-app" data-category="mobile-app">
                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/flight-formation.jpg" />
                </div>


        </section>

    </section>
    <section class="s-purchase bg-wight">
        <div class="container">
            <div class="columns">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">

                    <?php button('Purchase Now', '#', 'btn-cta level-item', '') ?>

                </div>
            </div>
        </div>
    </section>
</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
