<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content">
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">Classic Five Fullwidth</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Classic Five Fullwidth</a></li>
                        <!--                    <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>

    </section>
    <section class="s-projects s-team">
        <div class="container">
            <h2 class="title">Our Latest Projects</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="tab" id="tab">
                <ul class="tab-navigation">
                    <li><a href="#tabAll">All</a></li>
                    <li><a href="#webDesign">Web Design</a></li>
                    <li><a href="#mobileApp">Mobile App</a></li>
                    <li><a href="#illustration">Illustration</a></li>
                    <li><a href="#photography">Photography</a></li>
                </ul>

            </div>

        </div>
        <div class="column-wrapper">

            <div class="columns is-1">
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-5.png" alt="p">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>

                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>

                                    <span class="counts-like ">23</span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>

                                    <span class="counts-like">23</span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>

                                    <span class="counts-like">23</span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>
                                    <span class="counts-like">23</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns is-1">
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-5.png" alt="p">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>

                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>

                                    <span class="counts-like ">23</span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>

                                    <span class="counts-like">23</span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>

                                    <span class="counts-like">23</span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column is-3">
                    <div class="boxes">
                        <figure class="image">
                            <img src="/app/img/photo-6.png" alt="/">
                            <div class="boxes-hover">
                                <div class="icons">
                                    <a><i class="fas fa-search icon-link"></i></a>
                                    <a><i class="far fa-eye"></i></a>
                                </div>
                            </div>
                        </figure>
                        <div class="media-content">
                            <div class="content columns">
                                <p class="column is-10 column-aligne-r">
                                    <strong>John Smith</strong>

                                    Lorem ipsum dolor sit amet.
                                </p>
                                <p class="column is-2 column-aligne-r content-p">
                                    <a><strong><i class="far fa-heart"></i></strong></a>
                                    <span class="counts-like">23</span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="btn-cta level-item" style="">Load More</a>
    </section>
    <section class="s-purchase bg-wight">
        <div class="container">
            <div class="columns">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">

                    <?php button('Purchase Now', '#', 'btn-cta level-item', '') ?>

                </div>
            </div>
        </div>
    </section>
</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
