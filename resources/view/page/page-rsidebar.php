<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content page">

    <!-- Breadcrumbs container  start-->
    <section class="breadcrumbs breadcrumbs-full breadcrumbs-bg__img">
        <div class="container">
            <div class="wrapper-content text-centered">
                <h1 class="title title-page">Page right sidebar</h1>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Page right sidebar</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <div class="container multitype">
        <div class="tile is-ancestor">
            <div class="tile  is-parent">

                <div class="tile">
                    <div class="tile is-parent">
                        <article class="tile">
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis corporis dignissimos, est maiores nisi officia praesentium quam tenetur vero voluptas.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis corporis dignissimos, est maiores nisi officia praesentium quam tenetur vero voluptas.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consequatur cupiditate deserunt dolor earum eligendi esse est facere in incidunt labore, magnam, molestiae natus nostrum perferendis quo recusandae sequi sit vitae voluptate! Accusantium ad at consequatur culpa cum, dignissimos ducimus eveniet exercitationem explicabo facere harum in incidunt ipsam iusto minima mollitia neque non obcaecati officiis pariatur praesentium quidem recusandae rem repudiandae soluta, veritatis voluptas? Accusantium amet architecto deleniti dolore eaque earum eius eligendi eos esse fugit harum hic id illum in iste officia, quasi quos, ratione reprehenderit saepe ullam unde vero! Earum error ipsum libero modi molestias obcaecati vel, vitae.</p>
                            </div>

                        </article>

                    </div>

                </div>
            </div>
            <div class="tile is-vertical is-3 is-parent" id="sidebar">
                <div class="tile is-child">
                    <!-- Content -->
                    <div class="content">

                        <aside class="w">
                            <nav class="menu">
                                <div class="menu-search field has-addons">
                                    <p class="control control-wh has-icons-right">
                                        <input class="input is-large" type="text" placeholder="Search">
                                        <span class="icon is-small is-right">
                                     <i class="fas fa-search" aria-hidden="true"></i>
                                </span>
                                    </p>
                                </div>

                                <p class="menu-label">
                                    Recent posts
                                </p>
                                <div class="tabs-wrapper tabs-border">
                                    <div class="tabs menu-tabs">
                                        <ul role="tablist" class="tablist ml-0 mt-0 ui-tabs-nav  ui-helper-reset ui-helper-clearfix ui-widget-header tablist-widget-header">
                                            <li class="tablist-item is-active">
                                                <a class="tablist-item__link is-boxe__radius" href="#recent">Recent</a>
                                            </li>
                                            <li class="tablist-item">
                                                <a class="tablist-item__link" href="#popular">Popular</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="recent">
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        John Smith
                                                    </p>
                                                    <time datetime="2016-1-1" class="data">6 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-10.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Video post John
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 30 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-fotter.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Video post John
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 30 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-8.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Smith
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 1 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div id="popular" class="is-invisible">
                                    </div>
                                </div>

                                <p class="menu-label">
                                    Categories
                                </p>
                                <ul class="menu-list ml-0 category">

                                    <li class="category-item">
                                        <a class="category-item__link border-g">
                                    <span class="icon is-small">
                                        <i class="fas fa-caret-right" aria-hidden="true"></i>
                                    </span>
                                            <span>Payments</span>
                                        </a>
                                    </li>
                                    <li class="category-item">
                                        <a class="category-item__link border-g">
                                    <span class="icon is-small">
                                        <i class="fas fa-caret-right" aria-hidden="true"></i>
                                        </span><span>Transfers</span>
                                        </a>
                                    </li>

                                    <li class="category-item">
                                        <a class="category-item__link">
                                    <span class="icon is-small">
                                         <i class="fas fa-caret-right" aria-hidden="true"></i>
                                    </span>
                                            <span>Balance</span>
                                        </a>
                                    </li>
                                </ul>
                                <p class="menu-label">
                                    Tags Cloud
                                </p>
                                <div class="field is-grouped is-grouped-multiline ">
                                    <p class="control">
                                        <a class="button">
                                            One
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Two
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Three
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Four
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Five
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Size
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Seven
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Eight
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Nine
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Ten
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Eleven
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Twelve
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Thirteen
                                        </a>
                                    </p>
                                </div>
                                <p class="menu-label">
                                    Text Widget
                                </p>
                                <div class="content-widget">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur dolorem id
                                        perspiciatis quam temporibus. Commodi.</p>
                                </div>


                            </nav>
                        </aside>

                    </div>
                </div>
            </div>
        </div>



    </div>
    <!-- Breadcrumbs container  end-->

    <!-- Excellent container  start-->

    <!-- Excellent container  end-->

    <!-- Info container  start-->

    <!-- Info container  end-->
    <!-- Counters container  start-->

    <!-- Counters container  end-->

    <!-- Services container  start-->

    <!-- Services container  end-->

    <!-- Team container  start-->

    <!-- Team container  end-->

    <!-- Partners container  start-->

    <!-- Partners container  end-->
    <!-- Main container  end-->
</main>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
