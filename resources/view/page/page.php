<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content page">

    <!-- Breadcrumbs container  start-->
    <section class="breadcrumbs breadcrumbs-full breadcrumbs-bg__img">
        <div class="container">
            <div class="wrapper-content text-centered">
                <h1 class="title title-page">Page fullscreen</h1>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Page fullscreen</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <div class="container multitype">
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">

                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis corporis dignissimos, est maiores nisi officia praesentium quam tenetur vero voluptas.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent is-6">
                <article class="tile is-child">

                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aut corporis deleniti harum id ipsa iste labore, laborum magnam ut?Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                    </div>
                </article>
            </div>
        </div>

        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>

                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
        </div>

        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
        </div>

        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">

                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus ducimus eum excepturi hic maiores, natus officia temporibus. Accusantium ad at, consequatur cumque deleniti dignissimos dolores eaque, ex fugiat fugit inventore ipsum maiores molestias nulla, recusandae repudiandae sint suscipit vero voluptatibus?</p>
                    </div>
                </article>
            </div>

            <div class="tile is-parent is-6">
                <article class="tile is-child">

                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam enim eum quibusdam vero vitae. Ab accusantium asperiores culpa dolor et facere facilis id illum, incidunt ipsum iusto magnam molestiae natus neque perspiciatis porro provident ratione, repellendus reprehenderit similique sit velit!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam enim eum quibusdam vero vitae. Ab accusantium asperiores culpa dolor et facere facilis id illum, incidunt ipsum iusto magnam molestiae natus neque perspiciatis porro provident ratione, repellendus reprehenderit similique sit velit!</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">

                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi ducimus eius eos fugiat magnam maiores mollitia odio, perferendis qui repellat sit veritatis? Alias aperiam consectetur dolorem excepturi, in ipsum itaque, odio perspiciatis qui quisquam reiciendis reprehenderit velit veritatis vero.</p>
                    </div>
                </article>
            </div>
        </div>

        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, voluptates! Blanditiis, commodi eum ex expedita harum ipsa nisi similique.</p>
                    </div>
                </article>
            </div>
        </div>


    </div>
    <!-- Breadcrumbs container  end-->

    <!-- Excellent container  start-->

    <!-- Excellent container  end-->

    <!-- Info container  start-->

    <!-- Info container  end-->
    <!-- Counters container  start-->

    <!-- Counters container  end-->

    <!-- Services container  start-->

    <!-- Services container  end-->

    <!-- Team container  start-->

    <!-- Team container  end-->

    <!-- Partners container  start-->

    <!-- Partners container  end-->
    <!-- Main container  end-->
</main>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
