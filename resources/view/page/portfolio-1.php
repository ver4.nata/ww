<?php include PATCH . "resources/view/component/header-2.html"; ?>
<!-- Main container start -->
<main class="main-content search">
    <!--  container Breadcrumbs  start-->
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">Portfolio</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">Home</a></li>
                        <li><a class="breadcrumb-item" href="#">Ocean Rocks</a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <!--  container Breadcrumbs  end-->
    <div class="container multitype">

        <div class="tile is-ancestor">
            <div class="tile is-vertical">
                <section class="slider">
                    <div class='carousel carousel-animated carousel-animate-slide'>
                        <div class='carousel-container'>
                            <div class='carousel-item has-background'>
                                <img class="is-background" src="https://wikiki.github.io/images/merry-christmas.jpg" alt="" width="100%" height="200" />
                            </div>
<!--                            <div class='carousel-item has-background'>-->
<!--                                <img class="is-background" src="https://wikiki.github.io/images/singer.jpg" alt="" width="640" height="310" />-->
<!--                                <div class="title">Original Gift: Offer a song with <a href="https://lasongbox.com" target="_blank">La Song Box</a></div>-->
<!--                            </div>-->
<!--                            <div class='carousel-item has-background'>-->
<!--                                <img class="is-background" src="https://wikiki.github.io/images/sushi.jpg" alt="" width="640" height="310" />-->
<!--                                <div class="title">Sushi time</div>-->
<!--                            </div>-->
<!--                            <div class='carousel-item has-background'>-->
<!--                                <img class="is-background" src="https://wikiki.github.io/images/life.jpg" alt="" width="640" height="310" />-->
<!--                                <div class="title">Life</div>-->
<!--                            </div>-->
                        </div>
<!--                        <div class="carousel-navigation is-overlay">-->
<!--                            <div class="carousel-nav-left">-->
<!--                                <i class="fa fa-chevron-left" aria-hidden="true"></i>-->
<!--                            </div>-->
<!--                            <div class="carousel-nav-right">-->
<!--                                <i class="fa fa-chevron-right" aria-hidden="true"></i>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>

                </section>
                <section class="s-projects">
                    <div class="container">
                        <div class="columns is-1">
                            <div class="column is-8">
                                <h2 class="title">Our Latest Projects</h2>
                                <div class="boxes">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit repudiandae veniam voluptatibus. Accusamus accusantium assumenda, debitis, deserunt dolorem est ex facere iste molestias nemo officia quia quidem saepe tempore voluptatem.
                                </div>
                                <a href="#" class="btn-cta level-item" style="">Load More</a>
                            </div>
                            <div class="column is-4">
                                <div class="boxes">

                                    <div class="media-content">
                                        <div class="content columns">
                                            <p class="column is-10 column-aligne-r">
                                                <strong>John Smith</strong>

                                                Lorem ipsum dolor sit amet.
                                            </p>
                                            <p class="column is-2 column-aligne-r content-p">
                                                <a><strong><i class="far fa-heart"></i></strong></a>

                                                <span class="counts-like">23</span>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <section class="s-projects s-team">
                    <div class="container">
                        <h2 class="title">Our Latest Projects</h2>
                        <div class="columns is-1">
                            <div class="column is-4">
                                <div class="boxes">
                                    <figure class="image">
                                        <img src="/app/img/photo-5.png" alt="p">
                                        <div class="boxes-hover">
                                            <div class="icons">
                                                <a><i class="fas fa-search icon-link"></i></a>
                                                <a><i class="far fa-eye"></i></a>
                                            </div>
                                        </div>

                                    </figure>
                                    <div class="media-content">
                                        <div class="content columns">
                                            <p class="column is-10 column-aligne-r">
                                                <strong>John Smith</strong>

                                                Lorem ipsum dolor sit amet.
                                            </p>
                                            <p class="column is-2 column-aligne-r content-p">
                                                <a><strong><i class="far fa-heart"></i></strong></a>

                                                <span class="counts-like ">23</span>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="column is-4">
                                <div class="boxes">
                                    <figure class="image">
                                        <img src="/app/img/photo-6.png" alt="/">
                                        <div class="boxes-hover">
                                            <div class="icons">
                                                <a><i class="fas fa-search icon-link"></i></a>
                                                <a><i class="far fa-eye"></i></a>
                                            </div>
                                        </div>
                                    </figure>
                                    <div class="media-content">
                                        <div class="content columns">
                                            <p class="column is-10 column-aligne-r">
                                                <strong>John Smith</strong>

                                                Lorem ipsum dolor sit amet.
                                            </p>
                                            <p class="column is-2 column-aligne-r content-p">
                                                <a><strong><i class="far fa-heart"></i></strong></a>

                                                <span class="counts-like">23</span>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="column is-4">
                                <div class="boxes">
                                    <figure class="image">
                                        <img src="/app/img/photo.png" alt="/">
                                        <div class="boxes-hover">
                                            <div class="icons">
                                                <a><i class="fas fa-search icon-link"></i></a>
                                                <a><i class="far fa-eye"></i></a>
                                            </div>
                                        </div>
                                    </figure>
                                    <div class="media-content">
                                        <div class="content columns">
                                            <p class="column is-10 column-aligne-r">
                                                <strong>John Smith</strong>

                                                Lorem ipsum dolor sit amet.
                                            </p>
                                            <p class="column is-2 column-aligne-r content-p">
                                                <a><strong><i class="far fa-heart"></i></strong></a>

                                                <span class="counts-like">23</span>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>

    </div>


</main>

<!-- Main container end -->
<?php include PATCH . "resources/view/component/footer.php"; ?>
