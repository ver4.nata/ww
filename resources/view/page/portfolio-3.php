<?php include PATCH . "resources/view/component/header.html"; ?>
<!-- Main container start -->
<main class="main-content">
    <!--  container Сarousel  start-->
    <section class="s-carousel">
        <div class='carousel'>
            <div class='carousel-item'>
                <div class="carousel-item__img img-overlay">
                        <img  src="/app/img/island.jpg" alt="island" width="100%"/>
                    </div>
                    <div class="carousel-item__text cont-overlay">
                        <p class="title-t">Unique and Modern Design</p>
                        <h1 class="title">Portfolio PSD Template</h1>
                        <p class="title-b">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet
                            doming
                            id quod mazim placerat facer possim assum.</p>
                        <div class="bth-group">
                            <?php button('Get Started','#','btn-cta', '') ?>
                            <?php button('button text2','#','btn-cta btn-cta__noactive', '') ?>
                        </div>
                    </div>
            </div>
        </div>
        </div>
    </section>

    <!--  container Сarousel  end-->

    <!--  container INFO  start-->
    <section class="s-info">
        <div class='container container__text-center'>
            <h2 class="title">
                Title
            </h2>
            <p class="sub-title">Lorem ipsum dolor sit amet.</p>
            <div class="block">
                <p class="box__text">Lorem ipsum dolor sit amet um exercitationem fugit itaque minus molestias necessitatibus officiis optio perferendis possimus provident quisquam repudiandae sint temporibus ullam, veniam! A adipisci asperiores assumenda eligendi ex illo incidunt laudantium molestias nulla odit omnis optio quidem ratione, reprehenderit, sequi sit vel voluptas?</p>
                <div class="box__btn bth-group">
                    <?php button('lowed','#','btn-cta btn-cta__bege', '') ?>
                </div>
                <div class="box__img">
                    <a class="box__img-link" href="#"><img class="box__img-item" src="/app/img/heand-1.png" alt="heand-1"></a>
                    <a class="box__img-link" href="#"></a><img class="box__img-item" src="/app/img/heand-2.png" alt="heand-2">
                </div>
            </div>
        </div>
    </section>
    <!--  container IMFO  end-->
    <!--  container Сarousel  start-->
    <section class="s-carousel">
        <div class='container'>
            <div class="block">
                <div class="box__img">
                    <a class="box__img-link" href="#"><img class="box__img-item" src="/app/img/photo-5.png" alt="heand-1"></a>
                    <a class="box__img-link" href="#"><img class="box__img-item" src="/app/img/photo-4.png" alt="heand-1"></a>
                    <a class="box__img-link" href="#"></a><img class="box__img-item" src="/app/img/photo-6.png" alt="heand-2">
                </div>
            </div>
        </div>
    </section>
    <!--  container Сarousel  end-->
</main>

<!-- Main container end -->
<?php include PATCH . "resources/view/component/footer.php"; ?>
