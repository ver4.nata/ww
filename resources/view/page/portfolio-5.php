<?php include PATCH . "resources/view/component/header.html"; ?>
<!-- Main container start -->
<main class="main-content portfolio">
    <!--  container Сarousel  start-->
    <section class="s-carousel">
        <div class='carousel'>
            <div class='carousel-item'>
                <div class="carousel-item__img img-overlay">
                    <img src="/app/img/island.jpg" alt="island" width="100%"/>
                </div>
                <div class="carousel-item__text cont-overlay">
                    <p class="title-t">Unique and Modern Design</p>
                    <h1 class="title">Portfolio PSD Template</h1>
                    <p class="title-b">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet
                        doming
                        id quod mazim placerat facer possim assum.</p>
                    <div class="bth-group">
                        <?php button('Get Started', '#', 'btn-cta', '') ?>
                        <?php button('button text2', '#', 'btn-cta btn-cta__noactive', '') ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <!--  container Сarousel  end-->

    <!--  container INFO  start-->
    <section class="s-info">
        <div class='container'>
            <div class="columns">
                <div class="column">
                    <h2 class="title">
                        Title
                    </h2>
                    <p class="sub-title">Lorem ipsum dolor sit amet.</p>
                    <p class="box__text box__text-left">Lorem ipsum dolor sit amet um exercitationem fugit itaque minus
                        molestias necessitatibus officiis optio perferendis possimus provident quisquam repudiandae sint
                        temporibus ullam, veniam! A adipisci asperiores assumenda eligendi ex illo incidunt laudantium
                        molestias nulla odit omnis optio quidem ratione, reprehenderit, sequi sit vel voluptas?</p>
                    <div class="box__btn box__btn-left">
                        <?php button('lowed', '#', 'btn-cta btn-cta__bege', '') ?>
                    </div>
                </div>
                <div class="column is-3">
                    <div class="box__table content">
                        <table class="table is-fullwidth">
                            <tbody>
                            <tr class="table__tr">
                                <th class="table__th">Skills</th>
                                <td>City
                                </td>
                            </tr>
                            <tr class="table__tr">
                                <th class="table__th">Skills</th>
                                <td>City
                                </td>
                            </tr>
                            <tr class="table__tr">
                                <th class="table__th">Skills</th>
                                <td><a href="https://en.wikipedia.org/wiki/Leicester_City_F.C."
                                       title="Leicester City F.C.">Leicester City</a>
                                </td>
                            </tr>
                            <tr class="table__tr">
                                <th class="table__th">Skills</th>
                                <td><a href="https://en.wikipedia.org/wiki/Leicester_City_F.C."
                                       title="Leicester City F.C.">Leicester City</a>
                                </td>
                            </tr>
                            <tr class="table__tr">
                                <th class="table__th">Skills</th>
                                <td>City
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="s-excellent block block__grey">
        <div class="container">
            <div class="columns">
                <div class="column ">
                    <img class="img column-img" src="/app/img/iphone-in-hand.png" alt="iphone">
                </div>
                <div class="column column-text">
                    <h2 class="title">Excellent for Mobile Devices.</h2>
                    <p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                        Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>

                    <p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                        Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>

                </div>
            </div>


        </div>
    </section>
    <section class="block">
        <div class="container">
            <div class="columns">
                <div class="column column-text">
                    <h2 class="title">Excellent for Mobile Devices.</h2>
                    <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                        Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At aut beatae nesciunt quaerat quia sed sequi vitae. A aliquid at, deleniti dolores fuga iure, nulla optio possimus recusandae tempora ut.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At aut beatae nesciunt quaerat quia sed sequi vitae. A aliquid at, deleniti dolores fuga iure, nulla optio possimus recusandae tempora ut.</p>
                </div>
                <div class="column column-img">
                    <img class="column__img-item" src="/app/img/iphone-in-hand.png" alt="iphone">
                </div>
            </div>

        </div>
    </section>

    <!--  container IMFO  end-->
    <!--  container Сarousel  start-->
    <section class="carousel">


        <div class="block block__grey block__text-center">
            <p class="title">
                Title
            </p>
            <p class="sub-title">Lorem ipsum dolor sit amet.</p>
            <div class="box__img is-marginless">
                <a class="box__img-link" href="#"><img class="box__img-item" src="/app/img/photo-5.png"
                                                       alt="heand-1"></a>
                <a class="box__img-link" href="#"><img class="box__img-item" src="/app/img/photo-4.png"
                                                       alt="heand-1"></a>
                <a class="box__img-link" href="#"><img class="box__img-item" src="/app/img/photo-4.png"
                                                       alt="heand-1"></a>
                <a class="box__img-link" href="#"></a><img class="box__img-item" src="/app/img/photo-6.png"
                                                           alt="heand-2">
                <a class="box__img-link" href="#"></a><img class="box__img-item" src="/app/img/photo-6.png"
                                                           alt="heand-2">

            </div>
        </div>
    </section>
    <!--  container Сarousel  end-->
</main>

<!-- Main container end -->
<?php include PATCH . "resources/view/component/footer-2.php"; ?>
