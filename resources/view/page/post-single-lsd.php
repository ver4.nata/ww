<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content blog">

    <!-- Breadcrumbs container  start-->
    <section class="breadcrumbs breadcrumbs-full breadcrumbs-bg__img">
        <div class="container">
            <div class="wrapper-content text-centered">
                <h1 class="title title-page">Blog</h1>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Blog standart </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <div class="container multitype">
        <div class="tile is-ancestor">
            <div class="tile is-vertical is-9">
                <div class="tile is-parent">
                    <article class="tile is-child">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-2by1 is-marginless">
                                    <img src="/app/img/photo-6.png" alt="Placeholder image">
                                </figure>
                                <div class="card-lable">
                                    <span class="tag lable is-black label-number">14</span>
                                    <br>
                                    <span class="tag lable is-black label-month">Fev</span></div>
                            </div>
                            <div class="card-content pl-0 pr-0">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title is-4">Standard blog post</p>
                                        <div class="card-content__detalies">
                                            <span class="subtitle is-6 sub-title-opacity">Bu:  <span
                                                        class="value"> @johnsmith</span></span>
                                            <span class="sub-title-opacity"> On: <span class="value"><time
                                                            datetime="2016-1-1">February 14, 2015 </time> </span></span>
                                            <span class="subtitle is-6 sub-title-opacity">Comments: <span class="value">3</span></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="content col-info">
                                    <p><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Phasellus nec iaculis mauris.Lorem ipsum dolor sit amet, consectetur
                                            adipiscing elit.
                                            Phasellus nec iaculis mauris.</b> <a>@bulmaio</a>.
                                        <a href="#">#css</a> <a href="#">#responsive</a></p>
                                </div>
                                <div class="content">

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse ex impedit in incidunt ipsum iure neque ut voluptas voluptates. Alias culpa dolore eum eveniet excepturi facilis fugit incidunt ipsa laboriosam minima, modi qui soluta ullam voluptates voluptatibus. Consectetur, laudantium molestiae nesciunt numquam reprehenderit veritatis. Ad aliquid amet asperiores at atque aut blanditiis consectetur dicta, dolore doloremque dolorum ducimus enim excepturi expedita fuga fugiat fugit harum in ipsum itaque laudantium magnam magni, molestiae nobis nulla officia officiis possimus quidem quo repellat, reprehenderit repudiandae tenetur voluptatum! Accusantium ad aspernatur at, culpa dicta eius error, fugiat incidunt iusto maxime nam, necessitatibus officiis placeat quas quis repellendus ut? Asperiores assumenda at consequuntur corporis, debitis delectus earum error est et explicabo facere harum id itaque iusto laboriosam laborum libero modi mollitia nulla numquam odio officia quaerat ratione repellat sapiente sint temporibus totam, vel velit veniam. Assumenda eaque labore laborum modi mollitia necessitatibus officia optio, quaerat.</p>

                                    <blockquote class="blockquote">
                                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem et eum labore natus necessitatibus nisi quae reprehenderit sint tempora tenetur.</p>
                                        <span class="autor"> Lorem Ipsum Dolor</span>
                                    </blockquote>

                                </div>
                            </div>
                        </div>
                        <section class="hashtags">
                            <div class="field is-grouped is-grouped-multiline">
                                <div class="control">
                                    <div class="tags">

                                        <div class="tag tag-lable">
                                            <i class="fas fa-tags"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="control">
                                    <div class="tags">
                                        <a class="tag tag-link">CSS,</a>
                                    </div>
                                </div>
                                <div class="control">
                                    <div class="tags">
                                        <a class="tag tag-link">Flexbox,</a>

                                    </div>
                                </div>

                                <div class="control">
                                    <div class="tags">
                                        <a class="tag tag-link">Web Design</a>

                                    </div>
                                </div>

                            </div>
                        </section>
                        <section class="comments">
                            <p class="title">Comments 3</p>
                            <article class="media">
                                <figure class="media-left">
                                    <p class="image is-64x64">
                                        <img src="https://bulma.io/images/placeholders/128x128.png">
                                    </p>
                                </figure>
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <strong>Barbara Middleton</strong>
                                            <br>
                                            <small><a>Like</a> · <a>Reply</a> · 3 hrs</small>
                                            <br>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros
                                            lacus, nec ultricies elit blandit non. Suspendisse pellentesque mauris sit
                                            amet dolor blandit rutrum. Nunc in tempus turpis.


                                        </p>
                                    </div>

                                    <article class="media">
                                        <figure class="media-left">
                                            <p class="image is-48x48">
                                                <img src="https://bulma.io/images/placeholders/96x96.png">
                                            </p>
                                        </figure>
                                        <div class="media-content">
                                            <div class="content">
                                                <p>
                                                    <strong>Sean Brown</strong>
                                                    <br>
                                                    <small><a>Like</a> · <a>Reply</a> · 2 hrs</small>
                                                    <br>
                                                    Donec sollicitudin urna eget eros malesuada sagittis. Pellentesque
                                                    habitant morbi tristique senectus et netus et malesuada fames ac
                                                    turpis egestas. Aliquam blandit nisl a nulla sagittis, a lobortis
                                                    leo feugiat.


                                                </p>
                                            </div>

                                            <article class="media">
                                                Vivamus quis semper metus, non tincidunt dolor. Vivamus in mi eu lorem
                                                cursus ullamcorper sit amet nec massa.
                                            </article>

                                            <article class="media">
                                                Morbi vitae diam et purus tincidunt porttitor vel vitae augue. Praesent
                                                malesuada metus sed pharetra euismod. Cras tellus odio, tincidunt
                                                iaculis diam non, porta aliquet tortor.
                                            </article>
                                        </div>
                                    </article>

                                    <article class="media">
                                        <figure class="media-left">
                                            <p class="image is-48x48">
                                                <img src="https://bulma.io/images/placeholders/96x96.png">
                                            </p>
                                        </figure>
                                        <div class="media-content">
                                            <div class="content">
                                                <p>
                                                    <strong>Kayli Eunice </strong>
                                                    <br>
                                                    <small><a>Like</a> · <a>Reply</a> · 2 hrs</small>
                                                    <br>
                                                    Sed convallis scelerisque mauris, non pulvinar nunc mattis vel.
                                                    Maecenas varius felis sit amet magna vestibulum euismod malesuada
                                                    cursus libero. Vestibulum ante ipsum primis in faucibus orci luctus
                                                    et ultrices posuere cubilia Curae; Phasellus lacinia non nisl id
                                                    feugiat.


                                                </p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </article>
                            <article class="media">

                                <div class="media-content">
                                    <div class="field">
                                        <p class="control">
                                            <textarea class="textarea" placeholder="Add a comment..."></textarea>
                                        </p>
                                    </div>
                                    <div class="field">
                                        <p class="control">
                                            <button class="button">Post comment</button>
                                        </p>
                                    </div>
                                </div>
                            </article>
                        </section>
                    </article>


                </div>

            </div>
            <div class="tile tile-p is-parent" id="sidebar">
                <article class="tile is-child">
                    <!-- Content -->
                    <div class="content">

                        <aside class="w">
                            <nav class="menu">
                                <div class="menu-search field has-addons">
                                    <p class="control control-wh has-icons-right">
                                        <input class="input is-large" type="text" placeholder="Search">
                                        <span class="icon is-small is-right">
                                     <i class="fas fa-search" aria-hidden="true"></i>
                                </span>
                                    </p>
                                </div>

                                <p class="menu-label">
                                    Recent posts
                                </p>
                                <div class="tabs-wrapper tabs-border">
                                    <div class="tabs menu-tabs">
                                        <ul role="tablist" class="tablist ml-0 mt-0 ui-tabs-nav  ui-helper-reset ui-helper-clearfix ui-widget-header tablist-widget-header">
                                            <li class="tablist-item is-active">
                                                <a class="tablist-item__link is-boxe__radius" href="#recent">Recent</a>
                                            </li>
                                            <li class="tablist-item">
                                                <a class="tablist-item__link" href="#popular">Popular</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="recent">
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        John Smith
                                                    </p>
                                                    <time datetime="2016-1-1" class="data">6 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-10.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Video post John
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 30 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-fotter.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Video post John
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 30 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-8.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Smith
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 1 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div id="popular" class="is-invisible">
                                    </div>
                                </div>

                                <p class="menu-label">
                                    Categories
                                </p>
                                <ul class="menu-list ml-0 category">

                                    <li class="category-item">
                                        <a class="category-item__link border-g">
                                    <span class="icon is-small">
                                        <i class="fas fa-caret-right" aria-hidden="true"></i>
                                    </span>
                                            <span>Payments</span>
                                        </a>
                                    </li>
                                    <li class="category-item">
                                        <a class="category-item__link border-g">
                                    <span class="icon is-small">
                                        <i class="fas fa-caret-right" aria-hidden="true"></i>
                                        </span><span>Transfers</span>
                                        </a>
                                    </li>

                                    <li class="category-item">
                                        <a class="category-item__link">
                                    <span class="icon is-small">
                                         <i class="fas fa-caret-right" aria-hidden="true"></i>
                                    </span>
                                            <span>Balance</span>
                                        </a>
                                    </li>
                                </ul>
                                <p class="menu-label">
                                    Tags Cloud
                                </p>
                                <div class="field is-grouped is-grouped-multiline ">
                                    <p class="control">
                                        <a class="button">
                                            One
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Two
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Three
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Four
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Five
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Size
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Seven
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Eight
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Nine
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Ten
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Eleven
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Twelve
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Thirteen
                                        </a>
                                    </p>
                                </div>
                                <p class="menu-label">
                                    Text Widget
                                </p>
                                <div class="content-widget">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur dolorem id
                                        perspiciatis quam temporibus. Commodi.</p>
                                </div>


                            </nav>
                        </aside>
                    </div>
                </article>
            </div>
        </div>


    </div>


    </div>
    <!-- Breadcrumbs container  end-->

    <!-- Excellent container  start-->

    <!-- Excellent container  end-->

    <!-- Info container  start-->

    <!-- Info container  end-->
    <!-- Counters container  start-->

    <!-- Counters container  end-->

    <!-- Services container  start-->

    <!-- Services container  end-->

    <!-- Team container  start-->

    <!-- Team container  end-->

    <!-- Partners container  start-->

    <!-- Partners container  end-->
    <!-- Main container  end-->
</main>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
