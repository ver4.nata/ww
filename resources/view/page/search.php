<?php include PATCH . "resources/view/component/header-2.html"; ?>
<!-- Main container start -->
<main class="main-content search">
    <!--  container Breadcrumbs  start-->
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">Search</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">Home</a></li>
                        <li><a class="breadcrumb-item" href="#">Search results for "dfrdfr" </a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <!--  container Breadcrumbs  end-->
    <div class="container multitype">

        <div class="tile is-ancestor">
            <div class="tile is-vertical is-9">
                <div class="content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, omnis?</p>
                </div>
            </div>
            <div class="tile tile-p is-parent" id="sidebar">
                <article class="tile is-child">
                    <!-- Content -->
                    <div class="content">

                        <aside class="w">
                            <nav class="menu">
                                <div class="menu-search field has-addons">
                                    <p class="control control-wh has-icons-right">
                                        <input class="input is-large" type="text" placeholder="Search">
                                        <span class="icon is-small is-right">
                                     <i class="fas fa-search" aria-hidden="true"></i>
                                </span>
                                    </p>
                                </div>

                                <p class="menu-label">
                                    Recent posts
                                </p>
                                <div class="tabs-wrapper tabs-border">
                                    <div class="tabs menu-tabs">
                                        <ul role="tablist" class="tablist ml-0 mt-0 ui-tabs-nav  ui-helper-reset ui-helper-clearfix ui-widget-header tablist-widget-header">
                                            <li class="tablist-item is-active">
                                                <a class="tablist-item__link is-boxe__radius" href="#recent">Recent</a>
                                            </li>
                                            <li class="tablist-item">
                                                <a class="tablist-item__link" href="#popular">Popular</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="recent">
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        John Smith
                                                    </p>
                                                    <time datetime="2016-1-1" class="data">6 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-10.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Video post John
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 30 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-fotter.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Video post John
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 30 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="media">
                                            <figure class="media-left ml-0">
                                                <p class="image is-64x64 is-1by1">
                                                    <img src="/app/img/photo-8.png">
                                                </p>
                                            </figure>
                                            <div class="media-content">
                                                <div class="content">
                                                    <p>
                                                        Smith
                                                    </p>
                                                    <time datetime="2016-1-1" class="data"> 1 Jan 2016</time>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div id="popular" class="is-invisible">
                                    </div>
                                </div>

                                <p class="menu-label">
                                    Categories
                                </p>
                                <ul class="menu-list ml-0 category">

                                    <li class="category-item">
                                        <a class="category-item__link border-g">
                                    <span class="icon is-small">
                                        <i class="fas fa-caret-right" aria-hidden="true"></i>
                                    </span>
                                            <span>Payments</span>
                                        </a>
                                    </li>
                                    <li class="category-item">
                                        <a class="category-item__link border-g">
                                    <span class="icon is-small">
                                        <i class="fas fa-caret-right" aria-hidden="true"></i>
                                        </span><span>Transfers</span>
                                        </a>
                                    </li>

                                    <li class="category-item">
                                        <a class="category-item__link">
                                    <span class="icon is-small">
                                         <i class="fas fa-caret-right" aria-hidden="true"></i>
                                    </span>
                                            <span>Balance</span>
                                        </a>
                                    </li>
                                </ul>
                                <p class="menu-label">
                                    Tags Cloud
                                </p>
                                <div class="field is-grouped is-grouped-multiline ">
                                    <p class="control">
                                        <a class="button">
                                            One
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Two
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Three
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Four
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Five
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Size
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Seven
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Eight
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Nine
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Ten
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Eleven
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Twelve
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button">
                                            Thirteen
                                        </a>
                                    </p>
                                </div>
                                <p class="menu-label">
                                    Text Widget
                                </p>
                                <div class="content-widget">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur dolorem id
                                        perspiciatis quam temporibus. Commodi.</p>
                                </div>


                            </nav>
                        </aside>

                    </div>
                </article>
            </div>
        </div>

    </div>


</main>

<!-- Main container end -->
<?php include PATCH . "resources/view/component/footer.php"; ?>
