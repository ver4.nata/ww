<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content blog shop">

    <!-- Breadcrumbs container  start-->
    <section class="breadcrumbs breadcrumbs-full breadcrumbs-bg__img">
        <div class="container">
            <div class="wrapper-content text-centered">
                <h1 class="title title-page">Shop fullscreen</h1>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">home</a></li>
                        <li><a class="breadcrumb-item" href="#">Shop fullscreen 3</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <div class="container multitype">
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <section class="tile is-child">
                    <div class="select">
                        <select>
                            <option>Sort by</option>
                            <option>With options</option>
                        </select>
                    </div>
                    <div class="select">
                        <select>
                            <option>Show</option>
                            <option>With options</option>
                        </select>
                    </div>
                </section>

            </div>

        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/woo-ninja.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable ">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/woo-ninja.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable hiden">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old none">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/woo-ninja.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable hiden">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old none">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/woo-ninja.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable hiden">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old none">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/woo-ninja.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/woo-ninja.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable hiden">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old none">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/ninja-silhouette.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable hiden">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old none">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/woo-ninja.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable hiden">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old none">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child">
                    <div class="content">
                        <div class="card card__border">
                            <div class="card-image">
                                <figure class="image is-marginless">
                                    <img src="/app/img/ninja-silhouette.jpg" alt="Placeholder image">
                                </figure>
                                <div class="card-lable">
                                    <span class="tag lable is-danger">Sale</span>
                                </div>

                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="title">John Smith</p>
                                        <span class="price price-old">$ 12334</span>
                                        <span class="price">$ 123</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <section class="s-pagination">
                        <nav class="pagination" role="navigation" aria-label="pagination">
                            <ul class="pagination-list  pagination-list__centered style-none">
                                <li>
                                    <a class="pagination-link is-current" aria-label="Page 1" aria-current="page">1</a>
                                </li>
                                <li>
                                    <a class="pagination-link" aria-label="Goto page 2">2</a>
                                </li>
                                <li>
                                    <a class="pagination-link" aria-label="Goto page 3">-></a>
                                </li>
                            </ul>
                        </nav>
                    </section>
                </article>
            </div>

        </div>

    </div>


    <!-- Breadcrumbs container  end-->

    <!-- Excellent container  start-->

    <!-- Excellent container  end-->

    <!-- Info container  start-->

    <!-- Info container  end-->
    <!-- Counters container  start-->

    <!-- Counters container  end-->

    <!-- Services container  start-->

    <!-- Services container  end-->

    <!-- Team container  start-->

    <!-- Team container  end-->

    <!-- Partners container  start-->

    <!-- Partners container  end-->
    <!-- Main container  end-->
</main>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
