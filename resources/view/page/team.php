<?php include PATCH . "resources/view/component/header-2.html"; ?>

<!-- Main container start -->
<main class="main-content">
    <section class="breadcrumbs-full">
        <div class="container">
            <div class="wrapper-content">
                <p class="title title-page">OUR Team</p>
                <nav class="breadcrumb is-right" aria-label="breadcrumbs">
                    <ul>
                        <li><a class="breadcrumb-item" href="#">Bulma</a></li>
                        <li><a class="breadcrumb-item" href="#">Documentation</a></li>
                        <!--                    <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>-->
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <section class="s-projects s-team">
        <div class="container">
            <h2 class="title">Our Latest Projects</h2>
            <p class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
            <div class="tab" id="tab">
                <ul class="tab-navigation">
                    <li><a href="#tabAll">All</a></li>
                    <li><a href="#webDesign">Web Design</a></li>
                    <li><a href="#mobileApp">Mobile App</a></li>
                    <li><a href="#illustration">Illustration</a></li>
                    <li><a href="#photography">Photography</a></li>
                </ul>
                <div class="columns is-1">
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/528-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p  class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/529-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/536-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="columns is-1">
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/528-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p  class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/529-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/536-min.jpg" alt=""/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="columns is-1">
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/528-min.jpg" alt="avatar-t"/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p  class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/529-min.jpg" alt="John Smith"/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="column is-4" id="tabAll">
                        <div class="boxes">
                            <figure class="image-avatar">
                                <img class="img-circle" src="/app/img/536-min.jpg" alt="John Smith"/>

                            </figure>
                            <div class="team-content">
                                <div class="content">
                                    <p class="full-name"><strong>John Smith</strong></p>
                                    <p class="sub-title">Developer</p>
                                    <p class="info">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius</p>
                                    <nav class="level is-mobile is-center">
                                        <div class="level-is_center">
                                            <a class="level-item" aria-label="reply">
                                                 <span class="icon is-small">
                                                    <i class="fas fa-reply" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="retweet">
                                                <span class="icon is-small">
                                                      <i class="fas fa-retweet" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <a class="level-item" aria-label="like">
                                                <span class="icon is-small">
                                                  <i class="fas fa-heart" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </nav>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </section>
    <section class="s-purchase">
        <div class="container">
            <div class="columns">
                <div class="column is-10">
                    <p class="title"><span>Waxom</span> Multipurpose WordPress Theme</p>
                    <p class="sub-title">Don't Forget to Rate the Template. Thanks so much!</p>
                </div>
                <div class="column is-2 column-is-right">

                    <?php button('Purchase Now', '#', 'btn-cta level-item', '') ?>

                </div>
            </div>
        </div>
    </section>

</main>
<!-- Main container  end-->
</div>

<?php include PATCH . "resources/view/component/footer.php"; ?>
</html>
